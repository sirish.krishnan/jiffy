#################################################################
#
#          .==.        .==.
#         //`^\\      //^`\\
#        // ^ ^\(\__/)/^ ^^\\
#       //^ ^^ ^/6  6\ ^^ ^^\\
#      //^ ^^ ^ ( .. ) ^ ^^^ \\
#     // ^^ ^/\//v""v\\/\^ ^ ^\\         JIFFY!
#    // ^^/\/  / `~~` \  \/\^ ^\\        ~~~~~~
#    \\^ /    / ,    , \    \^ //
#     \\/    ( (      ) )    \//
#      ^      \ \.__./ /      ^
#             (((`  ')))
#
#
#  filename : base.py
#
#  purpose  : Has the core logic for performing dependency 
#             checks using GCD test as well as Banerjee's
#             test. Also has a transformation validity
#             checker to check if the code could be 
#             transformed for concurrentization, vectorization
#             etcetra...
#
#################################################################

# MODULES IMPORTED 
# ~~~~~~~~~~~~~~~~

import math
import logging
import copy
import sympy
import sys
from collections import defaultdict

#################################################################

# CLASS DEFINITIONS
# ~~~~~~~~~~~~~~~~~

# ================================================
# getdeps :
# ~~~~~~~~~
# This class is used to create a session for
# analyze all the dependencies present in a nested
# loop structure; given the statement numbers, 
# array name references, lhs/rhs location as well
# as index variable expressions.
# ================================================

class getdeps(object):

    def __init__(self):
        self.lhs = []
        self.rhs = []
        self.looptype = "sreg"
        self.deplist = defaultdict(list)   # of class type depinfo()
        self.deptestobj = deptest()
        self.flowdv = []
        self.vars = {}
        self.stmts = []

    def get_inverted_deplist(self, deplist):
        
        inverted_deplist = copy.deepcopy(deplist)

        for key in inverted_deplist:
            for item in inverted_deplist[key]:
                logging.info("item was " + str(item.direction[0]))
                item.direction[0].reverse()
                logging.info("item after inversion is " + str(item.direction[0]))

        return inverted_deplist
       

    # ================================================
    # reset : 
    # ~~~~~~~
    # Resets all the members of the class to initial
    # pristine state.
    # ================================================

    def reset(self):
        self.lhs = []
        self.rhs = []
        self.looptype = "sreg"
        self.deplist = defaultdict(list)
        self.deptestobj = deptest()
        self.flowdv = []
        self.vars = {}


    # ================================================
    # setlooptype : 
    # ~~~~~~~~~~~~~
    # Sets the loop type in order to determine the 
    # loop execution order DVs.
    # ================================================

    def setlooptype(self, looptype):
        self.looptype = looptype
        if (looptype == "sreg"):
            self.flowdv = {
                "12": ['<='],
                "21": ['<'],
                "11": ['<'],
                "22": ['<']
            }
        elif (looptype == "sif"):
            self.flowdv = {
                "12": ['<'],
                "21": ['<'],
                "11": ['<'],
                "22": ['<']
            }
        elif (looptype == "dreg"):
            self.flowdv = {
                "12": [['=','<='],['<','*']],
                "21": [['=','<'],['<','*']],
                "11": [['<','*'],['=','<']],
                "22": [['<','*']],
            }
        elif (looptype == "dif"):
            self.flowdv = {
                "12": [['=','<'],['<','*']],
                "21": [['=','<'],['<','*']],
            }
        else:
            self.looptype = "invalid"
            logging.info("Sorry Geezer ! You're out of luck again !")
            self.flowdv = {
                "INVALID_DV" : []
            }

    # ================================================
    # addindexvar : 
    # ~~~~~~~~~~~~~
    # Adds an index variable with its upper and lower
    # bounds, this is used for dependency analyses.
    # ================================================

    def addindexvar(self, varname, lb, ub, nk):
        self.vars[varname] = [lb, ub, nk]


    # ================================================
    # dump : 
    # ~~~~~~
    # Dumps all class members.
    # ================================================

    def dump(self):
        logging.info("\n\n<======= Dumping all getdeps() class members ======> \n")
        logging.info("Loop type is " + self.looptype)
        logging.info("Execution flow DVs are " + str(self.flowdv))
        logging.info("LHS list is : ")
        for node in self.lhs:
            node.dump()
        logging.info("RHS list is : ")
        for node in self.rhs:
            node.dump()
        logging.info("Dumping deptestobj contents :")
        self.deptestobj.dump()


    # ================================================
    # addref : 
    # ~~~~~~~~
    # Adds an expression set consisting of lhs/rhs
    # position, statement number and array name to
    # either of 2 lists lhs/rhs depending on choice.
    # ================================================

    def addref(self, position, stmtno, arrname, exprset):

        # Add to list of statements if not there
        if (stmtno not in self.stmts):
            self.stmts.append(stmtno)

        newnode = refnode(stmtno, arrname, exprset)
        if (position == 'lhs'):
            self.lhs.append(newnode)
        elif (position == 'rhs'):
            self.rhs.append(newnode)
        else:
            logging.info("Sorry Geezer, you're out of luck today !!")


    # ================================================
    # calcdeplist : 
    # ~~~~~~~~~~~~~
    # Uses another class with its associated functions
    # to perform dependency analyses on the contraints
    # and generate a final list of all dependencies
    # discovered for the given set of constraints.
    # ================================================

    def calcdeplist(self):

        status = 0

        if (self.lhs == [] or self.rhs == []):
            logging.info("Nothing to be done! LHS / RHS / both empty !!")
            return -1

        # Take each pair of LHS & RHS deps
        for lhsdep in self.lhs:
            for rhsdep in self.rhs:

                # Skip all those deps whose array names don't match
                if (lhsdep.arrayname != rhsdep.arrayname):
                    continue
                if (len(lhsdep.exprset) != len(rhsdep.exprset)):
                    status = -1
                    logging.info("Something is wrong ! The lhsdep & rhsdep list lengths don't match!")
                    break

                logging.info("\n\n===================> Array name is " + str(lhsdep.arrayname))
                logging.info("===================> LHS STMT is " + str(lhsdep.stmtno))
                logging.info("===================> LHS EXPR is " + str(lhsdep.exprset))
                logging.info("===================> RHS STMT is " + str(rhsdep.stmtno))
                logging.info("===================> RHS EXPR is " + str(rhsdep.exprset))

                # Otherwise, perform dependency analysis
                self.deptestobj.reset()
                for varname in self.vars:
                    self.deptestobj.setvar(varname, self.vars[varname][0],\
                                    self.vars[varname][1], self.vars[varname][2]) 
                for i in range(0,len(lhsdep.exprset)):
                    logging.info("Deptest iteration #" + str(i))
                    self.deptestobj.set_expr_constraints(lhsdep.exprset[i], rhsdep.exprset[i])
                    logging.info("Dumping deptestobj contents before starting dependency analysis:")
                    self.deptestobj.dump()
                    logging.info("Triggering evaluation of dependency vectors set above:")
                    self.deptestobj.evaluate_expr_constraints()
                logging.info("Triggering evaluation of cumulative dep PSI vector")
                self.deptestobj.evaluate_cumulative_constraints()

                cumulative_psi = self.deptestobj.cumulative_result

                # Assume that the execution order dependence is a True dependence for now
                deptype = "T" # 'T' for True flow dependence, 'F' for Anti-dependence, 

                depinfonode = depinfo()

                if (lhsdep.stmtno == rhsdep.stmtno):
                    if ("11" in self.flowdv):
                        final_deps = self.deptestobj.get_final_dv_with_execution_order(cumulative_psi,self.flowdv["11"])
                        # Remove redundant nesting []
                        if (final_deps != []):
                            final_deps = final_deps[0]

                        if (final_deps == []):
                            cumulative_psi = self.deptestobj.compute_inverse()
                            # Do not consider execution order to get antidependences within same statement
                            #final_deps = self.deptestobj.get_final_dv_with_execution_order(cumulative_psi,self.flowdv["11"])
                            final_deps = cumulative_psi
                            deptype = "F"

                    else:
                        final_deps = []

                elif (lhsdep.stmtno < rhsdep.stmtno):
                    final_deps = self.deptestobj.get_final_dv_with_execution_order(cumulative_psi,self.flowdv["12"])
                    if (final_deps == []):
                        cumulative_psi = self.deptestobj.compute_inverse()
                        final_deps = self.deptestobj.get_final_dv_with_execution_order(cumulative_psi,self.flowdv["21"])
                        deptype = "F"
                        
                elif (lhsdep.stmtno > rhsdep.stmtno):
                    final_deps = self.deptestobj.get_final_dv_with_execution_order(cumulative_psi,self.flowdv["21"])
                    if (final_deps == []):
                        cumulative_psi = self.deptestobj.compute_inverse()
                        final_deps = self.deptestobj.get_final_dv_with_execution_order(cumulative_psi,self.flowdv["12"])
                        deptype = "F"

                depinfonode.direction = final_deps
                depinfonode.deptype = deptype
                depinfonode.arrayname = lhsdep.arrayname

                if (deptype == 'T'):
                    depinfonode.stmt1 = lhsdep.stmtno
                    depinfonode.stmt2 = rhsdep.stmtno
                else:
                    depinfonode.stmt1 = rhsdep.stmtno
                    depinfonode.stmt2 = lhsdep.stmtno

                if (final_deps != []):
                    self.deplist[str(depinfonode.stmt1)+deptype+str(depinfonode.stmt2)].append(depinfonode)

        logging.critical("List of dependencies discovered :")
        for key in self.deplist:
            logging.critical("\tDependency is " + str(key))
            logging.critical("\tContributing nodes are :")
            for item in self.deplist[key]:
                item.dump()
        return status

    # ================================================
    # check_transformations : 
    # ~~~~~~~~~~~~~~~~~~~~~~~
    # Checks for validity of some transformations
    # on the given dependency list.
    # ================================================

    def check_transformations(self):
        print("\nI) TRANSFORMATION CHECKS")
        print("########################")
        print("\t - Parallelizable as is ? "+ str(self.check_parallelizable(self.deplist)))
        if (self.looptype[0] != 's'):
            print("\t - Parallelizable if inverted ? "+ str(self.check_inverted_parallelizable(self.deplist)))
        print("\t - Inner loop vectorizable as is ? "+ str(self.check_vectorizable(self.deplist)))

    # ================================================
    # check_inverted_parallelizable : 
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # Checks if parallelization could be performed 
    # if the loops are inverted (outer <-> inner)
    # ================================================

    def check_inverted_parallelizable(self,deplist):
        inverted_list = self.get_inverted_deplist(deplist)
        return self.check_parallelizable(inverted_list)

    # ================================================
    # check_vectorizable : 
    # ~~~~~~~~~~~~~~~~~~~~
    # Checks if vectorization could be performed 
    # on the code / not
    # ================================================

    def check_vectorizable(self,deplist):
        stmt_list = []
        final_string = ""
        vectorizable = {}
        node_splitting_list = []

        for stmtno in self.stmts:
            vectorizable[stmtno] = True

        if (self.looptype[0] == 's'):
            for key in deplist:
                for node in deplist[key]:
                    if (node.stmt1 == node.stmt2):
                        if (node.deptype != 'F'):
                            vectorizable[node.stmt1] = False
                        elif (node.deptype == 'F' and node.direction[0] == '>'):
                            vectorizable[node.stmt1] = False
                    else:
                        reverse_true_dep = str(node.stmt2)+'T'+str(node.stmt1)
                        reverse_anti_dep = str(node.stmt2)+'F'+str(node.stmt1)
                        if (reverse_true_dep in deplist or reverse_anti_dep in deplist):
                            vectorizable[node.stmt1] = False

                        # Node splitting possibility detection
                        if (node.deptype == 'T'):
                            if (reverse_anti_dep in deplist):
                                if node not in node_splitting_list:
                                    node_splitting_list.append(node)

                        elif (node.deptype == 'F'):
                            reverse_true_dep = str(node.stmt2)+'T'+str(node.stmt1)
                            if (reverse_true_dep in deplist):
                                if node not in node_splitting_list:
                                    node_splitting_list.append(node)

        else:
            for key in deplist:
                for node in deplist[key]:
                    if (node.stmt1 == node.stmt2):
                        # True deps with 2nd direction vector other than '=' not allowed
                        if (node.deptype != 'F' and node.direction[0][1] != '='):
                            vectorizable[node.stmt1] = False
                        # Antideps with direction vectors [*,<] (ignorable cycle) and [*,=] are ok
                        elif (node.deptype == 'F' and node.direction[0][1] == '>'):
                            vectorizable[node.stmt1] = False

                    # If statement numbers are not the same, we pass!
                    else:
                        pass

        for key in vectorizable:
            final_string = final_string + "\n\t\tStmt #" + str(key) + " : " + str(vectorizable[key])

        final_string = final_string + "\n\t\tNode splitting list : "

        for node in node_splitting_list:
            final_string = final_string + "\n\t\t" + str(node.stmt1) + ";" + str(node.stmt2)  

        final_string = final_string + "\n"

        return final_string

    # ================================================
    # check_parallelizable : 
    # ~~~~~~~~~~~~~~~~~~~~~~
    # Checks if parallelization could be performed 
    # on the outer loop / not; if not, it checks if 
    # the same could be achieved in the inner loop
    # if the outer loop is run sequentially
    # ================================================

    def check_parallelizable(self,deplist):
        if self.deplist is {}:
            return "Yes! No dependencies at all !!"

        index = 0
        two_dimensional = False
        skip_list = []
    
        deplistcopy = copy.deepcopy(deplist)

        if (self.looptype[0] == 's'):
            logging.info("This is a 1D loop")
            two_dimensional = False
        else:
            logging.info("This is a 2D loop")
            two_dimensional = True

        if (two_dimensional):
            parallelizable = [True, True]
        else:
            parallelizable = [True]

        for key in deplistcopy:
            for index in range(0,len(parallelizable)):
                for item in deplistcopy[key]:
                    if (two_dimensional):
                        if(item.direction[0][index] != '='):
                            parallelizable[index] = False
                            if (index == 0):
                                del deplistcopy[key]
                    else:
                        logging.info("1D : item.direction is " + str(item.direction))
                        if (item.direction[0] != '='):
                            parallelizable[0] = False

        if (two_dimensional):
            if (parallelizable[0] == True):
                return "Yes, outer loop is parallelizable!"
            else:
                if (parallelizable[1] == True):
                    return "Yes, the inner loop is parallelizable but the outer loop must be run sequentially!"
                else:
                    return "No!"
        else:
            if (parallelizable[0]):
                return "Yes, the loop is parallelizable!"
            else:
                return "No, the loop is not parallelizable!"

#################################################################

# ================================================
# refnode :
# ~~~~~~~~~
# This class encapsulates all information 
# related to a dependence constraint, which
# includes the array reference, its associated
# algebraic expression in terms of index variables
# and statement number.
# ================================================

class refnode(object):
    
    def __init__(self,stmtno=0,arrayname="",exprset=""):
        self.stmtno = stmtno
        self.arrayname = arrayname
        self.exprset = exprset
        
    def dump(self):
        logging.info("\tStatement number is " + str(self.stmtno))
        logging.info("\tArray name is " + str(self.arrayname))
        logging.info("\tExpression set is " + str(self.exprset))


#################################################################

# ================================================
# depinfo :
# ~~~~~~~~~
# This class captures all information related to
# the final list of dependencies calculated from
# the constraints. Here, each node in the 
# dependency list would have the fields present
# in this class's definition.
# ================================================

class depinfo(object):

    def __init__(self,stmt1=0,stmt2=0,deptype='',direction='',arrayname = ''):
        self.stmt1 = stmt1
        self.stmt2 = stmt2
        self.deptype = deptype
        self.direction = direction
        self.arrayname = arrayname

    def dump(self):
        logging.critical("\t\tStmt1 is " + str(self.stmt1))
        logging.critical("\t\tStmt2 is " + str(self.stmt2))
        logging.critical("\t\tDeptype is " + str(self.deptype))
        logging.critical("\t\tDirection is " + str(self.direction))
        logging.critical("\t\tArray name is " + str(self.arrayname))


#################################################################

# ================================================
# deptest :
# ~~~~~~~~~
# This class is used to create a session for
# setting up variables and expressions to perform
# Banerjee's test in an iterative manner
# ================================================

class deptest(object):

    def __init__(self):
        self.vars = {}              # Holds info about variables involved
        self.result = []            # Holds a cumulative list of dependence test results
        self.cumulative_result = [] # Holds a cumulative result of dependence tests
                                    # done over the 'result' array of DVs
        self.a = []                 # Holds the coefficients of 1st expr
        self.b = []                 # Holds the coefficients of 2nd expr
        self.u = [0]                # Holds the upper bound of each variable
        self.l = [0]                # Holds lower bound of each variable
        self.n = [0]                # Holds array of normalizing factors for each variable
        self.good = True            # Indicates whether further tests can be done / not

    # ================================================
    # reset :
    # ~~~~~~~
    # This function resets all members of the class
    # to the initiall NULL set
    # ================================================

    def reset(self):
        self.vars = {}      
        self.result = []    
        self.a = []         
        self.b = []         
        self.u = [0]        
        self.l = [0]        
        self.n = [0]        
        self.good = True    

    # ================================================
    # reset :
    # ~~~~~~~
    # This function creates a new index variable 
    # & records its lower and upper bounds
    # ================================================

    def setvar(self, name, lb, ub, n):
        self.vars[name] = [lb, ub, n]
        self.u.append(ub)
        self.l.append(lb)
        self.n.append(n)

    # ================================================
    # dump :
    # ~~~~~~
    # This function dumps the contents all internal 
    # state variables of the class
    # ================================================

    def dump(self):
        logging.info("Vars are :")
        logging.info(str(self.vars))
        logging.info("Array a is " + str(self.a))
        logging.info("Array b is " + str(self.b))
        logging.info("Array u is " + str(self.u))
        logging.info("Array l is " + str(self.l))
        logging.info("Array n is " + str(self.n))
        logging.info("\n")

    # ================================================
    # set_expr_constraints :
    # ~~~~~~~~~~~~~~~~~~~~~~
    # This function sets index constraints in the form
    # of an algebraic expression for use in 
    # Banerjee's test
    # ================================================

    def set_expr_constraints(self,a,b):
        self.a = []
        self.b = []
        varlist = []
        varstr = ""

        expra = sympy.sympify(a)
        exprb = sympy.sympify(b)

        logging.info("expra is " + str(expra))
        logging.info("exprb is " + str(exprb))

        if (a == "0"):
            allcoeffsa = [0]
        else:
            allcoeffsa = sympy.Poly(expra)

        if (b == "0"):
            allcoeffsb = [0]
        else:
            allcoeffsb = sympy.Poly(exprb)

        for key in self.vars:
            varlist.append(key)
        for var in varlist:
            varstr = varstr + str(var) + ","

        varstr = varstr[:len(varstr)-1]
        logging.info("Full variable list is " + varstr)

        symobjlst = sympy.symbols(varstr)

        logging.info("Symobjlst is " + str(symobjlst))

        if (not isinstance(symobjlst, tuple)):
            self.a.append(expra.coeff(symobjlst))
            self.b.append(exprb.coeff(symobjlst))
        else:
            for i in range(0, len(symobjlst)):
                self.a.append(expra.coeff(symobjlst[i]))
                self.b.append(exprb.coeff(symobjlst[i]))

        #consta = list(set(allcoeffsa.coeffs()) - set(self.a))
        if (a == "0"):
            coeffa_list = [0]
        else:
            coeffa_list = allcoeffsa.coeffs()

        if (b == "0"):
            coeffb_list = [0]
        else:
            coeffb_list = allcoeffsb.coeffs()

        logging.info("All coeffs of a are " + str(coeffa_list))
        logging.info("All coeffs of a are " + str(coeffb_list))
        logging.info("Coeffs of var in a are " + str(self.a))
        logging.info("Coeffs of var in b are " + str(self.b))

        for i in self.a:
            if i in coeffa_list:
                coeffa_list.remove(i)

        for i in self.b:
            if i in coeffb_list:
                coeffb_list.remove(i)

        logging.info("Pruned coeffa_list is " + str(coeffa_list))
        logging.info("Pruned coeffb_list is " + str(coeffb_list))

        #logging.info("Consta is " + str(consta))
        #if (consta == []):
        if (coeffa_list == []):
            self.a.insert(0,0)
        else:
            self.a.insert(0,coeffa_list[0])
            #self.a.insert(0,consta[0])
        logging.info("Array a is " + str(self.a))

        #constb = list(set(allcoeffsb.coeffs()) - set(self.b))
        #logging.info("Constb is " + str(constb))
        #if (constb == []):
        if (coeffb_list == []):
            self.b.insert(0,0)
        else:
            self.b.insert(0,coeffb_list[0])
            #self.b.insert(0,constb[0])
        logging.info("Array b is " + str(self.b))

    # ================================================
    # evaluate_expr_constraints :
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # This function invokes the API to perform 
    # Banerjee's test given the index constraints
    # ================================================

    def evaluate_expr_constraints(self):

        gcd_test_result = gcd_test(self.a,self.b)

        if (gcd_test_result and (self.good == True)):
            logging.critical("GCD test passed ! Shall perform Banerjee's test")
            self.good = True
        else:
            logging.critical("GCD test failed ! No further tests to be performed")
            self.good = False
            return []

        if (self.good):
            banerjee_test_result = banerjee_test(self.a,self.b,self.u,self.l,self.n)
            self.result.append(banerjee_test_result)
            logging.info("Result array is " + str(self.result))
            return banerjee_test_result

    # ================================================
    # evaluate_cumulative_constraints :
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # This function calculates the final PSI DV 
    # given all the previously calculated PSI DVs
    # ================================================

    def evaluate_cumulative_constraints(self):

        #cumulative_result = [len(self.vars) * ['*']]
        cumulative_result = []

        if (len(self.result) == 1):
            cumulative_result = (self.result[0])
        else:
            for i in range(0,len(self.result)-1):
                for j in range(1,len(self.result)):
                    for comp1 in self.result[i]:
                        for comp2 in self.result[j]:
                            dotp = dot_product(comp1,comp2)
                            cumulative_result.append(dotp)
        logging.info("Cumulative result is " + str(cumulative_result))
        self.cumulative_result = cumulative_result
        
    # ================================================
    # get_final_dv_with_execution_order :
    # ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    # This function performs a dot product of a given 
    # set of DVs computed using Banerjee's test with 
    # the DVs derived from the execution order to get 
    # the final DV which shall be used
    # ================================================

    def get_final_dv_with_execution_order(self,a,b):
    
        if (a == [] or b == []):
            return []

        final_result = []
        pruned_result = []
        
        logging.info("a is " + str(a))
        logging.info("b is " + str(b))

        # Handle the case where it's a single subscript DV
        if (type(a[0]) is not list) and (type(b[0]) is not list):
            dp_result = dot_product(a,b)
            logging.info("Dot product is " + str(dp_result))
            final_result.append(dp_result)
            logging.info("Final result is " + str(final_result))
            for dv in final_result:
                if dv != ['.']:
                    pruned_result.append(dv)
            logging.info("Pruned list is " + str(pruned_result))
            return pruned_result

        # Otherwise, handle the case with multiple subscript DV
        for res in a:
            for dv in b:
                dp_result = dot_product(res,dv)
                if (dp_result not in final_result):
                    final_result.append(dp_result)

        for dv in final_result:
            adddv = True
            for component in dv:
                if ('.' == component):
                    adddv = False
            if (adddv):
                pruned_result.append(dv)

        logging.info("After considering execution order,")
        logging.info("Final DVs are " + str(final_result))       
        logging.info("Pruned DVs are " + str(pruned_result))       
        return pruned_result

    # ================================================
    # compute_inverse :
    # ~~~~~~~~~~~~~~~~~
    # This function calculates the inverse of the  
    # cumulative PSI DV calculated earlier
    # ================================================

    def compute_inverse(self):
        return compute_inverse(self.cumulative_result)


#################################################################

# ================================================
# btest_node :
# ~~~~~~~~~~~~
# This class is used to create nodes in the 
# worklist used in Banerjee's test while analysing
# dependencies in a hierarchical manner
# ================================================

class btest_node(object):
    
    def __init__(self, indexstr=[], index=-1, height=0):
        self.str = indexstr
        self.index = index
        self.height = height

    def update(self,indexstr,index,height):
        self.index = index
        self.str = indexstr
        self.height = height

    def dump(self):
        logging.info("\tDump :")
        logging.info("\tStr is " + str(self.str))
        logging.info("\tIndex is " + str(self.index))
        logging.info("\tHeight is " + str(self.height))

#################################################################

# BASIC API
# ~~~~~~~~~

# ================================================
# list_dump :
# ~~~~~~~~~~~
# This function simply dumps the contents of a list 
# ================================================

def list_dump(lst):
    for i in lst:
        i.dump()

# ================================================
# gcd_test :
# ~~~~~~~~~
# This function computes the GCD of 2 arrays of 
# integers element-by-element and returns the 
# final result
# ================================================

def gcd_test(a,b):

    gcd_result = 0

    logging.critical("Performing GCD Test...")

    if (len(a) != len(b)):
        logging.error("The array lengths do not match !")
        return False
    elif (len(a) == 1):
        logging.warning("Array size is only 1, GCD shall be 0 !")
        return False

    a0 = a[0]
    b0 = b[0]
    logging.critical("a0 is "+ str(a0))
    logging.critical("b0 is "+ str(b0))

    for i in range(1,len(a)):
        logging.critical("Other members of a & b are : " + str(a[i]) + "," + str(b[i]))
        gcd_res = math.gcd(a[i],b[i])
        if (i == 1):
            gcd_result = gcd_res
        else:
            gcd_result = math.gcd(gcd_result,gcd_res) 
        logging.info("GCD so far is " + str(gcd_result))
    
    logging.critical("Final GCD is " + str(gcd_result))

    div_by_gcd = (b0-a0) % gcd_result
    is_test_pass = (div_by_gcd == 0)

    logging.critical("GCD Test <GCD | ( B0 - A0 )> is " + str(is_test_pass))

    return is_test_pass

# ================================================
# get_pos_part :
# ~~~~~~~~~~~~~~
# This function computes the positive part of a
# real number as defined by Banerjee's test
# ================================================

def get_pos_part(a):
    if (a >= 0):
        return a
    else:
        return 0

# ================================================
# get_neg_part :
# ~~~~~~~~~~~~~~
# This function computes the negative part of a
# real number as defined by Banerjee's test
# ================================================

def get_neg_part(a):
    if (a <= 0):
        return -a
    else:
        return 0

# ================================================
# banerjee_compute_bounds_array :
# ~~~~~~~~~~~~~~~~~~~~
# This function computes and returns the result of
# Banerjee's "less than" dependency test
# ================================================

def banerjee_compute_bounds_array(a,b,u,l,n):

    if (((len(a) + len(b) + len(u) + len(l) + len(n)) % 5) !=0 ):
        logging.error("The array lengths do not match !")
        return -1
    
    a0 = a[0]
    b0 = b[0]
    logging.info("a0 is "+ str(a0))
    logging.info("b0 is "+ str(b0))

    bounds_set = []

    for i in range(1,len(a)):
        
        logging.info("Iteration " + str(i))
        logging.info("Ak & Bk are : " + str(a[i]) + "," + str(b[i]))
        logging.info("Lk & Uk are : " + str(l[i]) + "," + str(u[i]))
        uk = u[i]
        lk = l[i]
        nk = n[i]
        ak = a[i]
        bk = b[i]
        ak_pos = get_pos_part(ak)
        ak_neg = get_neg_part(ak)
        bk_pos = get_pos_part(bk)
        bk_neg = get_neg_part(bk)
        logging.info("Ak_pos & Ak_neg are : " + str(ak_pos) + "," + str(ak_neg))
        logging.info("Bk_pos & Bk_neg are : " + str(bk_pos) + "," + str(bk_neg))

        lbk_star = (uk - lk) * (ak_neg - bk_pos) + (ak - bk)*lk 
        ubk_star = (uk - lk) * (ak_pos - bk_neg) + (ak - bk)*lk 
        #lbk_less = (uk - lk - nk) * get_neg_part(ak_neg - bk) + (ak - bk)*lk - bk*nk
        lbk_less = (uk - lk - nk) * (ak_neg - bk) + (ak - bk)*lk - bk*nk
        ubk_less = (uk - lk - nk) * get_pos_part(ak_pos - bk) + (ak - bk)*lk - bk*nk
        lbk_equals = (uk - lk) * get_neg_part(ak - bk) + (ak - bk)*lk 
        ubk_equals = (uk - lk) * get_pos_part(ak - bk) + (ak - bk)*lk 
        lbk_greater = (uk - lk - nk) * get_neg_part(ak - bk_pos) + (ak - bk)*lk + ak*nk
        ubk_greater = (uk - lk - nk) * get_pos_part(ak - bk_neg) + (ak - bk)*lk + ak*nk

        star_set = [lbk_star,ubk_star]
        less_set = [lbk_less,ubk_less]
        equals_set = [lbk_equals,ubk_equals]
        greater_set = [lbk_greater,ubk_greater]
        total_set = {
            "*" : star_set,
            "<" : less_set,
            "=" : equals_set,
            ">" : greater_set
        }
        bounds_set.append(total_set)

        diff = b0 - a0

        logging.info("LBK* was " + str(lbk_star))
        logging.info("UBK* was " + str(ubk_star))
        logging.info("LBK< was " + str(lbk_less))
        logging.info("UBK< was " + str(ubk_less))
        logging.info("LBK= was " + str(lbk_equals))
        logging.info("UBK= was " + str(ubk_equals))
        logging.info("LBK> was " + str(lbk_greater))
        logging.info("UBK> was " + str(ubk_greater))
        logging.info("Diff was " + str(diff))

        logging.info("Bounds set was " + str(bounds_set))

    return bounds_set

# ================================================
# compute_inverse :
# ~~~~~~~~~~~~~~~~~
# This function computes the inverse of a DV list
# ================================================

def compute_inverse(dv_list):
    
    if (dv_list == []):
        return []

    inverted_list = copy.deepcopy(dv_list)

    inverse_mapping = {
        '<' : '>',
        '=' : '=',
        '>' : '<',
        '<=': '>=',
        '>=': '<=',
        '!=': '!=',
        '*' : '*'
    }

    logging.info("DV list is " + str(dv_list))

    # If DV is single index in length
    if (type(dv_list[0]) is not list):
        for i in range(0,len(dv_list)):
            inverted_component = inverse_mapping[dv_list[i]]
            inverted_list[i] = inverted_component

    # If DV has multiple indices
    else:
        for i in range(0,len(inverted_list)):
            dv = inverted_list[i]
            for j in range(0,len(dv)):
                inverted_component = inverse_mapping[dv[j]]
                dv[j] = inverted_component

    logging.info("Inverted list is " + str(inverted_list))

    return inverted_list
    
# ================================================
# dot_product :
# ~~~~~~~~~~~~~
# This function performs the dot product of 
# 2 DVs according to some predefined rules.
# ================================================

def dot_product(x,y):

    dp_dict = {
        '<': {
              '<':'<',
              '=':'.',
              '>':'.',
              '<=':'<',
              '>=':'.',
              '!=':'<',
              '*':'<'
             },
        '=': {
              '<':'.',
              '=':'=',
              '>':'.',
              '<=':'=',
              '>=':'=',
              '!=':'.',
              '*':'='
             },
        '>': {
              '<':'.',
              '=':'.',
              '>':'>',
              '<=':'.',
              '>=':'>',
              '!=':'>',
              '*':'>'
             },
        '<=':{
              '<':'<',
              '=':'=',
              '>':'.',
              '<=':'<=',
              '>=':'=',
              '!=':'<',
              '*':'<='
             },
        '>=':{
              '<':'.',
              '=':'=',
              '>':'>',
              '<=':'=',
              '>=':'>=',
              '!=':'>',
              '*':'>='
             },
        '!=':{
              '<':'<',
              '=':'.',
              '>':'>',
              '<=':'<',
              '>=':'>',
              '!=':'!=',
              '*':'!='
             },
        '*': {
              '<':'<',
              '=':'=',
              '>':'>',
              '<=':'<=',
              '>=':'>=',
              '!=':'!=',
              '*':'*'
             }
    }

    dplist = []

    if (len(x) != len(y)): # Indicates single subscript DV
        for i in range(0,len(x)):
            for j in range(0,len(y)):
                xcord = x[i]
                ycord = y[j]
                dp = dp_dict[xcord][ycord]
                dplist.append(dp)
    else:
        for i in range (0, len(x)):
            xcord = x[i]
            ycord = y[i]
            dp = dp_dict[xcord][ycord]
            dplist.append(dp)

    logging.info("Dot product of " + str(x) + " and " + str(y) + " is " + str(dplist))

    return dplist

# ================================================
# banerjee_test :
# ~~~~~~~~~~~~~~~~~~~~
# This function performs Banerjee's test given 
# dependence equations involving index vars.
# Also, this test is performed in a hierarchical
# manner.
# ================================================

def banerjee_test(a,b,u,l,n):

    if (((len(a) + len(b) + len(u) + len(l) + len(n)) % 5) !=0 ):
        logging.error("The array lengths do not match !")
        return -1

    bounds_set = banerjee_compute_bounds_array(a,b,u,l,n)

    if (-1 == bounds_set):
        return -1

    worklist = []
    result_list = []
    pruned_list = []
    init_list = []
    lb = 0
    ub = 0

    for i in range (0,len(a)-1):
        init_list.append('*')

    logging.info("Subscript string is " + str(init_list))
    node = btest_node(init_list,-1,0) 
    worklist.append(node)

    if (len(node.str) == 1):
        logging.info("BT : Case I")
        #lst = ['*','<','>','=']
        lst = ['<','>','=']
        for i in lst:
            lb = bounds_set[0][i][0]
            ub = bounds_set[0][i][1]
            diff = b[0] - a[0]
            if (diff > ub or diff < lb):
                continue
            else:
                pruned_list.append(i)
    else:
        logging.info("BT : Case II")
        while True:
            if not worklist:
                break;
            else:
                node = worklist.pop(0)
                lb = 0
                ub = 0
                for j in range(0,len(node.str)):
                    lb = lb + bounds_set[j][node.str[j]][0]
                    ub = ub + bounds_set[j][node.str[j]][1]
                diff = b[0] - a[0]
    
                logging.info("LB and UB for " + str(node.str) + "are " + str(lb) + " & " + str(ub))
                index = node.index
                logging.info ("====> Length of str is " + str(len(node.str)))
    
                if index < len(node.str):
                    index = node.index + 1
                    logging.info("index is " + str(index))
                    lst = ['<','=','>']
    
                    # Ignore the case where LB & UB are both zero, those subscripts are
                    # probably 0 in 'a' as well as 'b'
                    if ((index < len(node.str)) and (bounds_set[index][node.str[j]][0] == 0)\
                                                and (bounds_set[index][node.str[j]][1] == 0)):
                        node.index = node.index+1
                        worklist.append(node)
                        continue
                    elif (diff > ub or diff < lb):
                        logging.info("The string " + str(node.str) + " does NOT satisfy the constraints")
                        continue
                    else:
                        result_list.append(node)
                        logging.info("The string " + str(node.str) + "satisfies the constraints")

                    for i in range(0,3):
                        newnode = btest_node()
                        if (index < len(node.str)):
                            newnode.str = copy.deepcopy(node.str)
                            newnode.index = index
                            newnode.height = node.height + 1
                            newnode.str[newnode.index] = lst[i]
                            worklist.append(newnode)

        logging.info ("BT : Result list is :")
        list_dump(result_list)
        logging.info ("Size of result list is :" + str(len(result_list)))

        max_height = -1
    
        for i in result_list:
            if i.height >= max_height:
                max_height = i.height
        logging.info ("Maxheight is :" + str(max_height))
    
        for i in result_list:
            if (i.height == max_height):
                pruned_list.append(i.str)
    
    logging.critical ("BT : Pruned list is :" + str(pruned_list))

    return pruned_list

#################################################################

# Test code
# ~~~~~~~~~

if __name__ == "__main__":

    numeric_level = getattr(logging, "INFO", None)
    logging.basicConfig(level=numeric_level)

    logging.info("\n")
    logging.info("======> Testing API gcd_test()")

    a = [1,18]
    b = [4,-9]
    logging.info(str(gcd_test(a,b)) + "\n")
    
    a = [1,2]
    b = [3,4,5,6]
    logging.info(str(gcd_test(a,b)) + "\n")

    a = [1,2]
    b = [3,4]
    logging.info(str(gcd_test(a,b)) + "\n")

    a = [0,1]
    b = [10,-1]
    logging.info(str(gcd_test(a,b)) + "\n")
    
    a = [0,1]
    b = [0,1]
    logging.info(str(gcd_test(a,b)) + "\n")

    a = [0,2]
    b = [3,4]
    logging.info(str(gcd_test(a,b)) + "\n")

    a = [0,1]
    b = [10,1]
    logging.info(str(gcd_test(a,b)) + "\n")

    logging.info("\n")
    logging.info("=======> Testing API get_pos_part()")

    a = 23
    logging.info(str(get_pos_part(a)) + "\n")

    a = -14
    logging.info(str(get_pos_part(a)) + "\n")

    a = 23
    logging.info(str(get_neg_part(a)) + "\n")

    a = -14
    logging.info(str(get_neg_part(a)) + "\n")

    logging.info("\n")
    logging.info("=======> Testing API banerjee_compute_bounds_array()")

    a = [0,1]
    b = [0,1]
    u = [0,10]
    l = [0,1]
    n = [1,1]
    logging.info(str(banerjee_compute_bounds_array(a,b,u,l,n)) + "\n")

    a = [0,2]
    b = [3,4]
    u = [0,10]
    l = [0,1]
    n = [1,1]
    logging.info(str(banerjee_compute_bounds_array(a,b,u,l,n)) + "\n")

    a = [0,1]
    b = [10,1]
    u = [0,10]
    l = [0,1]
    n = [1,1]
    logging.info(str(banerjee_compute_bounds_array(a,b,u,l,n)) + "\n")

    a = [0,10,1]
    b = [-1,10,1]
    u = [0,10,10]
    l = [0,1,1]
    n = [1,1,1]
    logging.info(str(banerjee_compute_bounds_array(a,b,u,l,n)) + "\n")

    a = [1,1,0]
    b = [0,1,0]
    u = [0,10,10]
    l = [0,1,1]
    n = [1,1,1]
    logging.info(str(banerjee_compute_bounds_array(a,b,u,l,n)) + "\n")

    a = [0,0,1]
    b = [1,0,1]
    u = [0,10,10]
    l = [0,1,1]
    n = [1,1,1]
    logging.info(str(banerjee_compute_bounds_array(a,b,u,l,n)) + "\n")

    a = [0,10,1]
    b = [-1,10,1]
    u = [0,10,10]
    l = [0,1,1]
    n = [1,1,1]
    logging.info(str(banerjee_compute_bounds_array(a,b,u,l,n)) + "\n")

    logging.info("\n")
    logging.info("Testing bannerjee_test() API")
    banerjee_test(a,b,u,l,n)

    a = [0,0,1]
    b = [1,0,1]
    u = [0,10,10]
    l = [0,1,1]
    n = [1,1,1]
    banerjee_test(a,b,u,l,n)

    a = [1,1,0]
    b = [0,1,0]
    u = [0,10,10]
    l = [0,1,1]
    n = [1,1,1]
    banerjee_test(a,b,u,l,n)

    a = [0,2]
    b = [3,4]
    u = [0,10]
    l = [0,1]
    n = [1,1]
    banerjee_test(a,b,u,l,n)

    a = [0,1]
    b = [0,1]
    u = [0,10]
    l = [0,1]
    n = [1,1]
    banerjee_test(a,b,u,l,n)

    x = ['*','*']
    y = ['<','>']
    dot_product(x,y)
   
    x = ['<','>']
    y = ['<','*']
    dot_product(x,y)

    x = ['=','<']
    y = ['=','<=']
    dot_product(x,y)

#################################################################
