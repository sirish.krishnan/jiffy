#################################################################
#
#          .==.        .==.
#         //`^\\      //^`\\
#        // ^ ^\(\__/)/^ ^^\\
#       //^ ^^ ^/6  6\ ^^ ^^\\
#      //^ ^^ ^ ( .. ) ^ ^^^ \\
#     // ^^ ^/\//v""v\\/\^ ^ ^\\         JIFFY!
#    // ^^/\/  / `~~` \  \/\^ ^\\        ~~~~~~
#    \\^ /    / ,    , \    \^ //
#     \\/    ( (      ) )    \//
#      ^      \ \.__./ /      ^
#             (((`  ')))
#
#
#  filename : sympy_test.py
#
#  purpose  : Script to test the Python SymPy library.
#
#################################################################

import sympy

i,j = sympy.symbols("i, j")
#expr = 8*i+j+2
#a = sympy.Poly(expr, i)
#print (a.coeffs())
#print (expr.coeff(i))
#print (expr.free_symbols)
#print (a.all_coeffs())
#
#lst = sympy.symbols("i, j")
#print (lst)

lst = sympy.symbols("i, j")
#expr = sympy.sympify("8*i + j + 2 + 10")
expr = sympy.sympify("-1+i+j")
b = sympy.Poly(expr)
print (b.coeffs())
print (expr.coeff(lst[0]))
print (expr.coeff(lst[1]))
