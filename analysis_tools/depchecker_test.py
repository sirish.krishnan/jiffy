#################################################################
#
#          .==.        .==.
#         //`^\\      //^`\\
#        // ^ ^\(\__/)/^ ^^\\
#       //^ ^^ ^/6  6\ ^^ ^^\\
#      //^ ^^ ^ ( .. ) ^ ^^^ \\
#     // ^^ ^/\//v""v\\/\^ ^ ^\\         JIFFY!
#    // ^^/\/  / `~~` \  \/\^ ^\\        ~~~~~~
#    \\^ /    / ,    , \    \^ //
#     \\/    ( (      ) )    \//
#      ^      \ \.__./ /      ^
#             (((`  ')))
#
#
#  filename : depchecker_test.py
#
#  purpose  : Script to test the dependency checker.
#
#################################################################

# MODULES IMPORTED 
# ~~~~~~~~~~~~~~~~

import base
import logging

#numeric_level = getattr(logging, "INFO", None)
#logging.basicConfig(level=numeric_level)
logging.info("\n")

getdepsobj = base.getdeps()

enable = 1

##################### EXAMPLE 1 ############################

'''
    for I = 1 to 10 do {
    S1: A(I) = ...
    S2: ... = A(I)
'''

if (enable == 0):
    getdepsobj.reset()
    getdepsobj.setlooptype("sreg")
    getdepsobj.addindexvar("i",1,10,1)
    getdepsobj.addref("lhs", 1, "A", ["i"])
    getdepsobj.addref("rhs", 2, "A", ["i"])
    getdepsobj.dump()
    getdepsobj.calcdeplist()

##################### EXAMPLE 2 ############################

'''
for I = 1 to 9 do {
S1: A(I) = ...
S2: ... = A(10-I)
}
'''

if (enable == 0):
    getdepsobj.reset()
    getdepsobj.setlooptype("sreg")
    getdepsobj.addindexvar("i",1,9,1)
    getdepsobj.addref("lhs", 1, "A", ["i"])
    getdepsobj.addref("rhs", 2, "A", ["10-i"])
    getdepsobj.dump()
    getdepsobj.calcdeplist()

##################### EXAMPLE 3 ############################

'''
for I = 1 to 10 do {
S1: A(2*I) = ...
S2: ... = A(4*I+3)
}
'''

if (enable == 0):
    getdepsobj.reset()
    getdepsobj.setlooptype("sreg")
    getdepsobj.addindexvar("i",1,10,1)
    getdepsobj.addref("lhs", 1, "A", ["2*i"])
    getdepsobj.addref("rhs", 2, "A", ["4*i+3"])
    getdepsobj.dump()
    getdepsobj.calcdeplist()

##################### EXAMPLE 4 ############################

'''
for I = 1 to 10 do {
S1: A(I) = ...
S2: ... = A(I+10)
}

Here the GCD test returns TRUE, but there are
actually no dependencies present.
'''

if (enable == 0):
    getdepsobj.reset()
    getdepsobj.setlooptype("sreg")
    getdepsobj.addindexvar("i",1,10,1)
    getdepsobj.addref("lhs", 1, "A", ["i"])
    getdepsobj.addref("rhs", 2, "A", ["i+10"])
    getdepsobj.dump()
    getdepsobj.calcdeplist()

##################### EXAMPLE 5 ############################

'''
for I = 1 to 10 do {
    for J = 1 to 10 do {
        S1 : A(I+1,J) = ...
        S2 : ...      = A(I,J+1)
    }
}
'''
if (enable == 0):
    getdepsobj.reset()
    getdepsobj.setlooptype("dreg")
    getdepsobj.addindexvar("i",1,10,1)
    getdepsobj.addindexvar("j",1,10,1)
    getdepsobj.addref("lhs", 1, "A", ["i+1","j"])
    getdepsobj.addref("rhs", 2, "A", ["i","j+1"])
    getdepsobj.dump()
    getdepsobj.calcdeplist()

##################### EXAMPLE 6 ############################

'''
for I = 1 to 10 do {
    for J = 1 to 10 do {
        S1 : A(I*10+J) = ...
        S2 : ... = A(I*10+J-1)
    }
}
'''

if (enable == 0):
    getdepsobj.reset()
    getdepsobj.setlooptype("dreg")
    getdepsobj.addindexvar("i",1,10,1)
    getdepsobj.addindexvar("j",1,10,1)
    getdepsobj.addref("lhs", 1, "A", ["i*10+j"])
    getdepsobj.addref("rhs", 2, "A", ["i*10+j-1"])
    getdepsobj.dump()
    getdepsobj.calcdeplist()

##################### EXAMPLE 7 ############################

'''
for I = 2 to 100 do {
    for J = 2 to 100 do {
        S1 : A(I,J) = B(I,J) + 2
        S2 : B(I,J) = A(I-1,J-1) - B(I,J)
    }
}
'''
if (enable == 0):
    getdepsobj.reset()
    getdepsobj.setlooptype("dreg")
    getdepsobj.addindexvar("i",2,100,1)
    getdepsobj.addindexvar("j",2,100,1)
    getdepsobj.addref("lhs", 1, "A", ["i","j"])
    getdepsobj.addref("rhs", 2, "A", ["i-1","j-1"])
    getdepsobj.addref("rhs", 1, "B", ["i","j"])
    getdepsobj.addref("lhs", 2, "B", ["i","j"])
    getdepsobj.addref("rhs", 2, "B", ["i","j"])
    getdepsobj.dump()
    getdepsobj.calcdeplist()

##################### EXAMPLE 8 ############################

'''
for I = 2 to 100 do {
    for J = 2 to 100 do {
        S1 : A(I,J) = B(I,J) + 2
        S2 : B(I,J) = A(I,J-1) - B(I,J)
    }
}
'''
if (enable == 0):
    getdepsobj.reset()
    getdepsobj.setlooptype("dreg")
    getdepsobj.addindexvar("i",2,100,1)
    getdepsobj.addindexvar("j",2,100,1)
    getdepsobj.addref("lhs", 1, "A", ["i","j"])
    getdepsobj.addref("rhs", 2, "A", ["i","j-1"])
    getdepsobj.addref("rhs", 1, "B", ["i","j"])
    getdepsobj.addref("lhs", 2, "B", ["i","j"])
    getdepsobj.addref("rhs", 2, "B", ["i","j"])
    getdepsobj.dump()
    getdepsobj.calcdeplist()

########################## FIN #############################

