#################################################################
#
#          .==.        .==.
#         //`^\\      //^`\\
#        // ^ ^\(\__/)/^ ^^\\
#       //^ ^^ ^/6  6\ ^^ ^^\\
#      //^ ^^ ^ ( .. ) ^ ^^^ \\
#     // ^^ ^/\//v""v\\/\^ ^ ^\\         JIFFY!
#    // ^^/\/  / `~~` \  \/\^ ^\\        ~~~~~~
#    \\^ /    / ,    , \    \^ //
#     \\/    ( (      ) )    \//
#      ^      \ \.__./ /      ^
#             (((`  ')))
#
#
#  filename : rough_tests.py
#
#  purpose  : Sloppy test script to check if the dependency
#             checker is working OK / not.
#
#################################################################

# MODULES IMPORTED 
# ~~~~~~~~~~~~~~~~

import base
import logging

numeric_level = getattr(logging, "INFO", None)
logging.basicConfig(level=numeric_level)
logging.info("\n")

deptest = base.deptest()

print ("\n ############## Resetting expressions ########### \n")

deptest.reset()
deptest.setvar('i', 1, 10, 1) 
deptest.setvar('j', 1, 10, 1) 
deptest.set_expr_constraints("i+1","i")
deptest.dump()
deptest.evaluate_expr_constraints()
deptest.set_expr_constraints("j","j+1")
deptest.dump()
deptest.evaluate_expr_constraints()
deptest.evaluate_cumulative_constraints()

print ("\n ############## Resetting expressions ########### \n")
deptest.reset()
deptest.setvar('i', 1, 10, 1) 
deptest.set_expr_constraints("i","i+10")
deptest.dump()
deptest.evaluate_expr_constraints()
deptest.evaluate_cumulative_constraints()

print ("\n ############## Resetting expressions ########### \n")
deptest.reset()
deptest.setvar('i', 1, 10, 1) 
deptest.set_expr_constraints("2*i","4*i+3")
deptest.dump()
deptest.evaluate_expr_constraints()
deptest.evaluate_cumulative_constraints()

print ("\n ############## Resetting expressions ########### \n")
deptest.reset()
deptest.setvar('i', 1, 9, 1) 
deptest.set_expr_constraints("i","10-i")
deptest.dump()
deptest.evaluate_expr_constraints()
deptest.evaluate_cumulative_constraints()

print ("\n ############## Resetting expressions ########### \n")
deptest.reset()
deptest.setvar('i', 1, 10, 1) 
deptest.set_expr_constraints("i","i")
deptest.dump()
deptest.evaluate_expr_constraints()
deptest.evaluate_cumulative_constraints()

print ("\n ############## Resetting expressions ########### \n")
deptest.reset()
deptest.setvar('i', 1, 10, 1) 
deptest.setvar('j', 1, 10, 1) 
deptest.set_expr_constraints("10*i+j","10*i+j-1")
deptest.dump()
print (deptest.evaluate_expr_constraints())
deptest.evaluate_cumulative_constraints()
flow_vec = [['=','<='],['=','<'],['<','*']]
deptest.get_final_dv_with_execution_order(deptest.cumulative_result,flow_vec)
deptest.compute_inverse()

print ("\n ############## Resetting expressions ########### \n")
deptest.reset()
deptest.setvar('i', 1, 10, 1) 
deptest.set_expr_constraints("i+1","i")
deptest.dump()
print (deptest.evaluate_expr_constraints())
deptest.evaluate_cumulative_constraints()

print ("\n ############## Resetting expressions ########### \n")
deptest.reset()
deptest.setvar('i', 1, 100, 1) 
deptest.setvar('j', 1, 100, 1) 
deptest.set_expr_constraints("3*j-2","3*j-2")
deptest.dump()
print (deptest.evaluate_expr_constraints())
deptest.evaluate_cumulative_constraints()

print ("\n ############## Resetting expressions ########### \n")
deptest.reset()
deptest.setvar('i', 1, 100, 1) 
deptest.setvar('j', 1, 100, 1) 
deptest.set_expr_constraints("i","i")
print (deptest.evaluate_expr_constraints())
deptest.set_expr_constraints("j+1","j")
deptest.dump()
print (deptest.evaluate_expr_constraints())
deptest.evaluate_cumulative_constraints()

print ("\n ############## Resetting expressions ########### \n")
deptest.reset()
deptest.setvar('i', 1, 100, 1) 
deptest.setvar('j', 1, 100, 1) 
deptest.set_expr_constraints("i+1","i")
print (deptest.evaluate_expr_constraints())
deptest.set_expr_constraints("j","j")
deptest.dump()
print (deptest.evaluate_expr_constraints())
deptest.evaluate_cumulative_constraints()

print ("\n ############## Resetting expressions ########### \n")
deptest.reset()
deptest.setvar('i', 1, 100, 1) 
deptest.setvar('j', 1, 100, 1) 
deptest.set_expr_constraints("i+1","i")
print (deptest.evaluate_expr_constraints())
deptest.set_expr_constraints("j+1","j")
deptest.dump()
print (deptest.evaluate_expr_constraints())
deptest.evaluate_cumulative_constraints()

print ("\n ############## Resetting expressions ########### \n")
deptest.reset()
deptest.setvar('i', 1, 100, 1) 
deptest.setvar('j', 1, 100, 1) 
deptest.setvar('k', 1, 100, 1) 
deptest.set_expr_constraints("i","i")
print (deptest.evaluate_expr_constraints())
deptest.set_expr_constraints("j+1","j")
deptest.dump()
print (deptest.evaluate_expr_constraints())
deptest.evaluate_cumulative_constraints()

print ("\n ############## Resetting expressions ########### \n")
deptest.reset()
deptest.setvar('j', 2, 101, 1) 
deptest.set_expr_constraints("j","j-1")
deptest.dump()
print (deptest.evaluate_expr_constraints())
deptest.evaluate_cumulative_constraints()

print ("\n ############## Resetting expressions ########### \n")
deptest.reset()
deptest.setvar('i', 1, 100, 1) 
deptest.setvar('j', 1, 100, 1) 
deptest.set_expr_constraints("i+1","i")
print (deptest.evaluate_expr_constraints())
deptest.set_expr_constraints("j","j+1")
deptest.dump()
print (deptest.evaluate_expr_constraints())
deptest.evaluate_cumulative_constraints()
flow_vec = [['=','<='],['=','<'],['<','*']]
deptest.get_final_dv_with_execution_order(deptest.cumulative_result,flow_vec)

print ("\n ############## Resetting expressions ########### \n")
deptest.reset()
deptest.setvar('i', 1, 100, 1) 
deptest.setvar('j', 1, 100, 1) 
deptest.set_expr_constraints("i","i+1")
print (deptest.evaluate_expr_constraints())
deptest.set_expr_constraints("j+1","j")
deptest.dump()
print (deptest.evaluate_expr_constraints())
deptest.evaluate_cumulative_constraints()
flow_vec = [['=','<='],['=','<'],['<','*']]
deptest.get_final_dv_with_execution_order(deptest.cumulative_result,flow_vec)
deptest.compute_inverse()

print ("\n ############## Resetting expressions ########### \n")
deptest.reset()
deptest.setvar('i', 1, 100, 1) 
deptest.setvar('j', 1, 100, 1) 
deptest.set_expr_constraints("i","i-1")
print (deptest.evaluate_expr_constraints())
deptest.set_expr_constraints("j","j-1")
deptest.dump()
print (deptest.evaluate_expr_constraints())
deptest.evaluate_cumulative_constraints()
flow_vec = [['=','<='],['=','<'],['<','*']]
deptest.get_final_dv_with_execution_order(deptest.cumulative_result,flow_vec)

print ("\n ############## Resetting expressions ########### \n")
deptest.reset()
deptest.setvar('i', 1, 100, 1) 
deptest.set_expr_constraints("i","i-1")
print (deptest.evaluate_expr_constraints())
deptest.dump()
deptest.evaluate_cumulative_constraints()
flow_vec = ['<']
deptest.get_final_dv_with_execution_order(deptest.cumulative_result,flow_vec)

getdepsobj = base.getdeps()
getdepsobj.setlooptype("dreg")
getdepsobj.addindexvar("i",1,100,1)
getdepsobj.addindexvar("j",1,100,1)
getdepsobj.addref("lhs", 1, "A", ["i+1","j"])
getdepsobj.addref("rhs", 2, "A", ["i","j+1"])
getdepsobj.dump()
getdepsobj.calcdeplist()

getdepsobj = base.getdeps()
getdepsobj.setlooptype("dreg")
getdepsobj.addindexvar("i",1,100,1)
getdepsobj.addindexvar("j",1,100,1)
getdepsobj.addref("lhs", 1, "A", ["i","j"])
getdepsobj.addref("rhs", 2, "A", ["i-1","j-1"])
getdepsobj.addref("rhs", 1, "B", ["i","j"])
getdepsobj.addref("lhs", 2, "B", ["i","j"])
getdepsobj.addref("rhs", 2, "B", ["i","j"])
getdepsobj.dump()
getdepsobj.calcdeplist()

'''
# Misc.
########
getdepsobj.reset()
getdepsobj.setlooptype("sreg")
getdepsobj.addindexvar("i",2,100,1)
getdepsobj.addref("lhs", 1, "X", ["i-1"])
getdepsobj.addref("rhs", 1, "X", ["i"])
getdepsobj.dump()
getdepsobj.calcdeplist()

getdepsobj.reset()
getdepsobj.setlooptype("dreg")
getdepsobj.addindexvar("i",1,100,1)
getdepsobj.addindexvar("j",1,100,1)
getdepsobj.addref("lhs", 1, "A", ["i","j+1"])
getdepsobj.addref("rhs", 1, "A", ["i","j"])
getdepsobj.dump()
getdepsobj.calcdeplist()

'''
