#################################################################
#
#          .==.        .==.
#         //`^\\      //^`\\
#        // ^ ^\(\__/)/^ ^^\\
#       //^ ^^ ^/6  6\ ^^ ^^\\
#      //^ ^^ ^ ( .. ) ^ ^^^ \\
#     // ^^ ^/\//v""v\\/\^ ^ ^\\         JIFFY!
#    // ^^/\/  / `~~` \  \/\^ ^\\        ~~~~~~
#    \\^ /    / ,    , \    \^ //
#     \\/    ( (      ) )    \//
#      ^      \ \.__./ /      ^
#             (((`  ')))
#
#
#  filename : demo5_ignorablecycles.py
#
#  purpose  : Script to demonstrate that vectorization can
#             be performed if ignorable cycles are present in 
#             the code.
#
#################################################################

# Example from Slide #3 :
'''

    for (i = 1 to N-4)
    {
        X(i) = X(i+4) * 10
    }

This program can be vectorized as :

    X(1:N-4) = X(5:N) * 10

'''

from time import time              # Import time tools
import sys
sys.path.append('../')
import pyopencl as cl              # Import the OpenCL GPU computing API
import numpy as np                 # Import number tools
import logging
from analysis_tools import base

######################################################################################################

numeric_level = getattr(logging, "INFO", None)
logging.basicConfig(level=numeric_level)
logging.info("\n")

print("\n\nDEMO5 : Performing dependency checks")
print("####################################")

getdepsobj = base.getdeps()
getdepsobj.reset()
getdepsobj.setlooptype("sreg")
getdepsobj.addindexvar("i",1,100,1)

getdepsobj.addref("lhs", 1, "A", ["i"])
getdepsobj.addref("rhs", 1, "A", ["i+1"])

#getdepsobj.setlooptype("dreg")
#getdepsobj.addindexvar("j",1,100,1)
#getdepsobj.addref("lhs", 1, "A", ["i","j"])
#getdepsobj.addref("rhs", 1, "A", ["i+1","j"])

getdepsobj.dump()
getdepsobj.calcdeplist()

print("\n\nDEMO5 : Performing transformation analyses")
print("##########################################")
getdepsobj.check_transformations()

######################################################################################################

MAX_WIDTH  = 12

a = np.full(MAX_WIDTH,0x0).astype(np.int32)  # Create an array to add
c = np.full(1,0x0).astype(np.int32)  # Create a random array to add

for i in range(MAX_WIDTH):
    a[i] = i*10

for i in range(MAX_WIDTH):
    print("INIT : Iteration #{}: a[{}] is {}".format(i,i,a[i]))

######################################################################################################

def gpu_array_sum_naive(a):
    context = cl.create_some_context()  # Initialize the Context
    queue = cl.CommandQueue(context, properties=cl.command_queue_properties.PROFILING_ENABLE)  # Instantiate a Queue with profiling (timing) enabled
    a_buffer = cl.Buffer(context, cl.mem_flags.READ_WRITE | cl.mem_flags.COPY_HOST_PTR, hostbuf=a)
    program = cl.Program(context, """
    __kernel void sum(__global int *a, unsigned width)
    {
        int i = 0;

        for (i = 0; i<width-4; i++)
        {
            a[i] = a[i+4] * 10;
        }

    }""").build()  # Compile the device program

    gpu_start_time = time()  # Get the GPU start time
    event = program.sum(queue, c.shape, None, a_buffer, np.uint32(MAX_WIDTH))  # Enqueue the GPU sum program XXX
    event.wait()  # Wait until the event finishes XXX
    gpu_end_time = time()  # Get the GPU end time
    c_gpu = np.empty_like(a)  # Create an empty array the same size as array a
    cl.enqueue_read_buffer(queue, a_buffer, c_gpu).wait()  # Read back the data from GPU memory into array c_gpu
    timediff = gpu_end_time - gpu_start_time
    #print("GPU Time: {0} s".format(gpu_end_time - gpu_start_time))  # Print the time the GPU program took, including both memory copies
    return c_gpu,timediff

######################################################################################################

def gpu_array_sum_improved(a):
    context = cl.create_some_context()  # Initialize the Context
    queue = cl.CommandQueue(context, properties=cl.command_queue_properties.PROFILING_ENABLE)  # Instantiate a Queue with profiling (timing) enabled
    a_buffer = cl.Buffer(context, cl.mem_flags.READ_WRITE | cl.mem_flags.COPY_HOST_PTR, hostbuf=a)
    program = cl.Program(context, """
    __kernel void sum(__global int4 *a, unsigned width)
    {
        int i = 0;

        for (i = 0; i < width/4-1; i++)
            a[i] = a[i+1] * 10;

    }""").build()  # Compile the device program

    gpu_start_time = time()  # Get the GPU start time
    event = program.sum(queue, (1,), None, a_buffer, np.uint32(MAX_WIDTH))  # Enqueue the GPU sum program XXX
    event.wait()  # Wait until the event finishes XXX
    gpu_end_time = time()  # Get the GPU end time
    c_gpu = np.empty_like(a)  # Create an empty array the same size as array a
    cl.enqueue_read_buffer(queue, a_buffer, c_gpu).wait()  # Read back the data from GPU memory into array c_gpu
    timediff = gpu_end_time - gpu_start_time
    #print("GPU Time: {0} s".format(gpu_end_time - gpu_start_time))  # Print the time the GPU program took, including both memory copies
    return c_gpu,timediff

######################################################################################################

# Test program starts here
##########################

c_gpu,timediff = gpu_array_sum_naive(a)  # Call the function that sums two arrays on the GPU

print("\n\nI) Naive computation :")
print("###################")
print("Time taken in seconds was : " + str(timediff))
print("I) Dimensions of modified a array is " + str(c_gpu.shape) )
print("Modified a was: {}".format(c_gpu))

for i in range(len(c_gpu)):
    print("\tI) Iteration #" + str(i) + "; a is " + str(c_gpu[i]))

e_gpu,timediff = gpu_array_sum_improved(a)  # Call the function that sums two arrays on the GPU

print("\n\nII) Vectorized computation :")
print("#######################")
print("Time taken in seconds was : " + str(timediff))
print("II) Dimensions of modified a array is " + str(e_gpu.shape))

print("Modified a was: {}".format(e_gpu))

for i in range(len(c_gpu)):
    print("\tII) Iteration #" + str(i) + "; a is " + str(e_gpu[i]))

a_diff = c_gpu - e_gpu

print("\n\nDifference in results of I and II")
print("##################################")
print("a_diff is {}".format(a_diff))
