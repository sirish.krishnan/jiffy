#################################################################
#
#          .==.        .==.
#         //`^\\      //^`\\
#        // ^ ^\(\__/)/^ ^^\\
#       //^ ^^ ^/6  6\ ^^ ^^\\
#      //^ ^^ ^ ( .. ) ^ ^^^ \\
#     // ^^ ^/\//v""v\\/\^ ^ ^\\         JIFFY!
#    // ^^/\/  / `~~` \  \/\^ ^\\        ~~~~~~
#    \\^ /    / ,    , \    \^ //
#     \\/    ( (      ) )    \//
#      ^      \ \.__./ /      ^
#             (((`  ')))
#
#
#  filename : demo3_2d_loopinversion.py
#
#  purpose  : Script to demonstrate concurrentization
#             after loop inversion & the outer loop is 
#             run sequentially.
#
#################################################################

# Example from Slide #21 :
'''

    for (i = 1 to N-1)
    {
        for (j= 1 to N-1)
        {
            S1: A(i,j+1) = B(i,j) + 10;
            S2: B(i+1,j+1) = A(i,j) + 5;
        }
    }

For this loop, interchange of 'i' and 'j'
loops is possible and the inner 'i' can 
be parallelized.

'''

from time import time              # Import time tools
import sys
sys.path.append('../')
import pyopencl as cl              # Import the OpenCL GPU computing API
import numpy as np                 # Import number tools
import logging
from analysis_tools import base
 
######################################################################################################

#numeric_level = getattr(logging, "INFO", None)
#logging.basicConfig(level=numeric_level)
#logging.info("\n")

print("\n\nDEMO3 : Performing dependency checks")
print("####################################")

getdepsobj = base.getdeps()
getdepsobj.reset()
getdepsobj.setlooptype("dreg")
getdepsobj.addindexvar("i",1,100,1)
getdepsobj.addindexvar("j",1,100,1)
getdepsobj.addref("lhs", 1, "A", ["i","j+1"])
getdepsobj.addref("rhs", 2, "A", ["i","j"])
getdepsobj.addref("lhs", 2, "B", ["i+1","j+1"])
getdepsobj.addref("rhs", 1, "B", ["i","j"])
getdepsobj.dump()
getdepsobj.calcdeplist()

print("\n\nDEMO2 : Performing transformation analyses")
print("##########################################")
getdepsobj.check_transformations()

######################################################################################################

MAX_WIDTH  = 100
MAX_HEIGHT = 100

a = np.full(MAX_WIDTH*MAX_HEIGHT,0x90).astype(np.int32)  # Create an array to add
b = np.full(MAX_WIDTH*MAX_HEIGHT,0x50).astype(np.int32)  # Create an array to add
c = np.full(1,0x0).astype(np.int32)  # Create a random array to add

######################################################################################################

def gpu_array_sum_naive(a, b):
    context = cl.create_some_context()  # Initialize the Context
    queue = cl.CommandQueue(context, properties=cl.command_queue_properties.PROFILING_ENABLE)  # Instantiate a Queue with profiling (timing) enabled
    a_buffer = cl.Buffer(context, cl.mem_flags.READ_WRITE | cl.mem_flags.COPY_HOST_PTR, hostbuf=a)
    b_buffer = cl.Buffer(context, cl.mem_flags.READ_WRITE | cl.mem_flags.COPY_HOST_PTR, hostbuf=b)
    program = cl.Program(context, """
    __kernel void sum(__global int *a, __global int *b, unsigned width, unsigned height)
    {
        int i = 0;
        int j = 0;

        for (i = 0; i<height-1; i++)
        {
            for (j = 0; j<width-1; j++)
            {   
                *(b+i*width+j+1) = *(a+i*width+j) + 10;
                *(a+(i+1)*width+j+1) = *(b+i*width+j) + 5;
            }
        }

    }""").build()  # Compile the device program

    gpu_start_time = time()  # Get the GPU start time
    event = program.sum(queue, c.shape, None, a_buffer, b_buffer, np.uint32(MAX_WIDTH), np.uint32(MAX_HEIGHT))  # Enqueue the GPU sum program XXX
    event.wait()  # Wait until the event finishes XXX
    gpu_end_time = time()  # Get the GPU end time
    c_gpu = np.empty_like(a)  # Create an empty array the same size as array a
    d_gpu = np.empty_like(b)  # Create an empty array the same size as array a
    cl.enqueue_read_buffer(queue, a_buffer, c_gpu).wait()  # Read back the data from GPU memory into array c_gpu
    cl.enqueue_read_buffer(queue, b_buffer, d_gpu).wait()  # Read back the data from GPU memory into array c_gpu
    timediff = gpu_end_time - gpu_start_time
    #print("GPU Time: {0} s".format(gpu_end_time - gpu_start_time))  # Print the time the GPU program took, including both memory copies
    return c_gpu,d_gpu,timediff

######################################################################################################

def gpu_array_sum_improved(a, b, j):
    context = cl.create_some_context()  # Initialize the Context
    queue = cl.CommandQueue(context, properties=cl.command_queue_properties.PROFILING_ENABLE)  # Instantiate a Queue with profiling (timing) enabled
    a_buffer = cl.Buffer(context, cl.mem_flags.READ_WRITE | cl.mem_flags.COPY_HOST_PTR, hostbuf=a)
    b_buffer = cl.Buffer(context, cl.mem_flags.READ_WRITE | cl.mem_flags.COPY_HOST_PTR, hostbuf=b)
    program = cl.Program(context, """
    __kernel void sum(__global int *a, __global int *b, unsigned j, unsigned width)
    {
        int i = get_global_id(0);
        
        if (i<99)
        {
            *(b+i*width+j+1) = *(a+i*width+j) + 10;
            *(a+(i+1)*width+j+1) = *(b+i*width+j) + 5;
        }

    }""").build()  # Compile the device program

    gpu_start_time = time()  # Get the GPU start time
    event = program.sum(queue, (MAX_HEIGHT,), None, a_buffer, b_buffer, np.uint32(j), np.uint32(MAX_WIDTH))  # Enqueue the GPU sum program XXX
    event.wait()  # Wait until the event finishes XXX
    gpu_end_time = time()  # Get the GPU end time
    c_gpu = np.empty_like(a)  # Create an empty array the same size as array a
    d_gpu = np.empty_like(b)  # Create an empty array the same size as array a
    cl.enqueue_read_buffer(queue, a_buffer, c_gpu).wait()  # Read back the data from GPU memory into array c_gpu
    cl.enqueue_read_buffer(queue, b_buffer, d_gpu).wait()  # Read back the data from GPU memory into array c_gpu
    timediff = gpu_end_time - gpu_start_time
    #print("GPU Time: {0} s".format(gpu_end_time - gpu_start_time))  # Print the time the GPU program took, including both memory copies
    return c_gpu,d_gpu,timediff

######################################################################################################

# Test program starts here
##########################

c_gpu,d_gpu,timediff = gpu_array_sum_naive(a, b)  # Call the function that sums two arrays on the GPU

print("\n\nI) Naive computation :")
print("###################")
print("Time taken in seconds was : " + str(timediff))
print ("I) Dimensions of modified a & b arrays are " + str(c_gpu.shape) + " and " + str(d_gpu.shape))
print("Modified a was: {}".format(c_gpu))
print("Modified b was: {}".format(d_gpu))

#for i in range(len(c_gpu)):
#    print("\tI) Iteration #" + str(i) + "; a is " + str(c_gpu[i]) + "; b is " + str(d_gpu[i]))

# Trying parallelized version :
###############################

timearray = []

# Outer loop sequential, inner loop in parallel
for j in range(0, MAX_HEIGHT-1):
    e_gpu,f_gpu,timediff = gpu_array_sum_improved(a, b, j)  
    a = e_gpu
    b = f_gpu
    timearray.append(timediff)
    print("Timediff of {}th iteration is {}".format(j,timediff))

print("\n\nII) Parallelized computation :")
print("#######################")
print("Time taken in seconds was : " + str(sum(timearray)))
print ("II) Dimensions of modified a & b arrays are " + str(e_gpu.shape) + " and " + str(f_gpu.shape))

print("Modified a was: {}".format(e_gpu))
print("Modified b was: {}".format(f_gpu))

#for i in range(len(c_gpu)):
#    print("\tII) Iteration #" + str(i) + "; a is " + str(e_gpu[i]) + "; b is " + str(f_gpu[i]))

a_diff = c_gpu - e_gpu
b_diff = d_gpu - f_gpu

print("\n\nDifference in results of I and II")
print("##################################")
print("a_diff is {}".format(a_diff))
print("b_diff is {}".format(b_diff))
