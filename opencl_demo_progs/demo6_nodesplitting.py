#################################################################
#
#          .==.        .==.
#         //`^\\      //^`\\
#        // ^ ^\(\__/)/^ ^^\\
#       //^ ^^ ^/6  6\ ^^ ^^\\
#      //^ ^^ ^ ( .. ) ^ ^^^ \\
#     // ^^ ^/\//v""v\\/\^ ^ ^\\         JIFFY!
#    // ^^/\/  / `~~` \  \/\^ ^\\        ~~~~~~
#    \\^ /    / ,    , \    \^ //
#     \\/    ( (      ) )    \//
#      ^      \ \.__./ /      ^
#             (((`  ')))
#
#
#  filename : demo6_nodesplitting.py
#
#  purpose  : Script to demonstrate node splitting detection and
#             emulating vectorization via concurrentization to
#             show performance improvement after node splitting.
#
#################################################################


# Example from Slide #78 :
'''

    for (i = 1 to 100)
    {
        S0: A(i) = X(i+1) + X(i)
        S1: X(i+1) = B(i)
    }

Node splitting can be done for the above 2 statements to give :

    for (i = 1 to 100)
    {
        S0: T(i)   = X(i+1)
        S1: A(i)   = T(i) + X(i)
        S2: X(i+1) = B(i)
    }
 
Final vectorizable code :

    S0 : T(1:100) = X(2:101)
    S1 : X(2:101) = B(1:100)
    S2 : A(1:100) = T(1:100) + X(1:100)

'''

from time import time              # Import time tools
import sys
sys.path.append('../')
import pyopencl as cl              # Import the OpenCL GPU computing API
import numpy as np                 # Import number tools
import logging
from analysis_tools import base

######################################################################################################

numeric_level = getattr(logging, "INFO", None)
logging.basicConfig(level=numeric_level)
logging.info("\n")

print("\n\nDEMO6 : Performing dependency checks")
print("####################################")

getdepsobj = base.getdeps()
getdepsobj.reset()

getdepsobj.setlooptype("sreg")
getdepsobj.addindexvar("i",1,100,1)
getdepsobj.addref("lhs", 2, "A", ["i+1"])
getdepsobj.addref("rhs", 1, "A", ["i"])
getdepsobj.addref("rhs", 1, "A", ["i+1"])

getdepsobj.dump()
getdepsobj.calcdeplist()

print("\n\nDEMO6 : Performing transformation analyses")
print("##########################################")
getdepsobj.check_transformations()

######################################################################################################

MAX_WIDTH  = 10

a = np.full(MAX_WIDTH,0x0).astype(np.int32)  # Create an array to add
b = np.full(MAX_WIDTH,0x0).astype(np.int32)  # Create an array to add
x = np.full(MAX_WIDTH,0x0).astype(np.int32)  # Create an array to add
t = np.full(MAX_WIDTH,0x0).astype(np.int32)  # Create an array to add
c = np.full(1,0x0).astype(np.int32)  # Create a random array to add

for i in range(MAX_WIDTH):
    a[i] = i*3
    b[i] = i+10
    x[i] = i*2

for i in range(MAX_WIDTH):
    print("INIT : Iteration #{}: a is {}, b is {}, x is {}".format(i,a[i],b[i],x[i]))

######################################################################################################

def gpu_array_sum_naive(a,b,x):
    context = cl.create_some_context()  # Initialize the Context
    queue = cl.CommandQueue(context, properties=cl.command_queue_properties.PROFILING_ENABLE)  # Instantiate a Queue with profiling (timing) enabled
    a_buffer = cl.Buffer(context, cl.mem_flags.READ_WRITE | cl.mem_flags.COPY_HOST_PTR, hostbuf=a)
    b_buffer = cl.Buffer(context, cl.mem_flags.READ_WRITE | cl.mem_flags.COPY_HOST_PTR, hostbuf=b)
    x_buffer = cl.Buffer(context, cl.mem_flags.READ_WRITE | cl.mem_flags.COPY_HOST_PTR, hostbuf=x)
    program = cl.Program(context, """
    __kernel void sum(__global int *a, __global int *b, __global int *x, unsigned width)
    {
        int i = 0;

        for (i = 0; i<width-1; i++)
        {
            a[i] = x[i+1] + x[i];
            x[i+1] = b[i];
        }

    }""").build()  # Compile the device program

    gpu_start_time = time()  # Get the GPU start time
    event = program.sum(queue, c.shape, None, a_buffer, b_buffer, x_buffer, np.uint32(MAX_WIDTH))  # Enqueue the GPU sum program XXX
    event.wait()  # Wait until the event finishes XXX
    gpu_end_time = time()  # Get the GPU end time
    a_gpu = np.empty_like(a)  # Create an empty array the same size as array a
    b_gpu = np.empty_like(b)  # Create an empty array the same size as array a
    x_gpu = np.empty_like(x)  # Create an empty array the same size as array a
    cl.enqueue_read_buffer(queue, a_buffer, a_gpu).wait()  # Read back the data from GPU memory into array c_gpu
    cl.enqueue_read_buffer(queue, b_buffer, b_gpu).wait()  # Read back the data from GPU memory into array c_gpu
    cl.enqueue_read_buffer(queue, x_buffer, x_gpu).wait()  # Read back the data from GPU memory into array c_gpu
    timediff = gpu_end_time - gpu_start_time
    #print("GPU Time: {0} s".format(gpu_end_time - gpu_start_time))  # Print the time the GPU program took, including both memory copies
    return a_gpu,b_gpu,x_gpu,timediff

######################################################################################################

def gpu_array_sum_improved_part1(t,x):
    context = cl.create_some_context()  # Initialize the Context
    queue = cl.CommandQueue(context, properties=cl.command_queue_properties.PROFILING_ENABLE)  # Instantiate a Queue with profiling (timing) enabled
    t_buffer = cl.Buffer(context, cl.mem_flags.READ_WRITE | cl.mem_flags.COPY_HOST_PTR, hostbuf=t)
    x_buffer = cl.Buffer(context, cl.mem_flags.READ_WRITE | cl.mem_flags.COPY_HOST_PTR, hostbuf=x)
    program = cl.Program(context, """
    __kernel void sum(__global int *t, __global int *x, unsigned width)
    {
        int i = get_global_id(0);

        if (i < width - 1)
        {
            t[i] = x[i+1];
        }

    }""").build()  # Compile the device program

    gpu_start_time = time()  # Get the GPU start time
    event = program.sum(queue, (MAX_WIDTH,), None, t_buffer, x_buffer, np.uint32(MAX_WIDTH))  # Enqueue the GPU sum program XXX
    event.wait()  # Wait until the event finishes XXX
    gpu_end_time = time()  # Get the GPU end time
    x_gpu = np.empty_like(a)  # Create an empty array the same size as array a
    t_gpu = np.empty_like(a)  # Create an empty array the same size as array a
    cl.enqueue_read_buffer(queue, x_buffer, x_gpu).wait()  # Read back the data from GPU memory into array c_gpu
    cl.enqueue_read_buffer(queue, t_buffer, t_gpu).wait()  # Read back the data from GPU memory into array c_gpu
    timediff = gpu_end_time - gpu_start_time
    #print("GPU Time: {0} s".format(gpu_end_time - gpu_start_time))  # Print the time the GPU program took, including both memory copies
    return t_gpu,x_gpu,timediff

######################################################################################################

def gpu_array_sum_improved_part2(b,x):
    context = cl.create_some_context()  # Initialize the Context
    queue = cl.CommandQueue(context, properties=cl.command_queue_properties.PROFILING_ENABLE)  # Instantiate a Queue with profiling (timing) enabled
    b_buffer = cl.Buffer(context, cl.mem_flags.READ_WRITE | cl.mem_flags.COPY_HOST_PTR, hostbuf=b)
    x_buffer = cl.Buffer(context, cl.mem_flags.READ_WRITE | cl.mem_flags.COPY_HOST_PTR, hostbuf=x)
    program = cl.Program(context, """
    __kernel void sum(__global int *b, __global int *x, unsigned width)
    {
        int i = get_global_id(0);

        if (i < width - 1)
        {
            x[i+1] = b[i];
        }

    }""").build()  # Compile the device program

    gpu_start_time = time()  # Get the GPU start time
    event = program.sum(queue, (MAX_WIDTH,), None, b_buffer, x_buffer, np.uint32(MAX_WIDTH))  # Enqueue the GPU sum program XXX
    event.wait()  # Wait until the event finishes XXX
    gpu_end_time = time()  # Get the GPU end time
    x_gpu = np.empty_like(a)  # Create an empty array the same size as array a
    b_gpu = np.empty_like(a)  # Create an empty array the same size as array a
    cl.enqueue_read_buffer(queue, x_buffer, x_gpu).wait()  # Read back the data from GPU memory into array c_gpu
    cl.enqueue_read_buffer(queue, b_buffer, b_gpu).wait()  # Read back the data from GPU memory into array c_gpu
    timediff = gpu_end_time - gpu_start_time
    #print("GPU Time: {0} s".format(gpu_end_time - gpu_start_time))  # Print the time the GPU program took, including both memory copies
    return b_gpu,x_gpu,timediff

######################################################################################################

def gpu_array_sum_improved_part3(a,t,x):
    context = cl.create_some_context()  # Initialize the Context
    queue = cl.CommandQueue(context, properties=cl.command_queue_properties.PROFILING_ENABLE)  # Instantiate a Queue with profiling (timing) enabled
    a_buffer = cl.Buffer(context, cl.mem_flags.READ_WRITE | cl.mem_flags.COPY_HOST_PTR, hostbuf=a)
    t_buffer = cl.Buffer(context, cl.mem_flags.READ_WRITE | cl.mem_flags.COPY_HOST_PTR, hostbuf=t)
    x_buffer = cl.Buffer(context, cl.mem_flags.READ_WRITE | cl.mem_flags.COPY_HOST_PTR, hostbuf=x)
    program = cl.Program(context, """
    __kernel void sum(__global int *a, __global int *t, __global int *x, unsigned width)
    {
        int i = get_global_id(0);

        if (i < width - 1)
        {
            a[i] = t[i] + x[i];
        }

    }""").build()  # Compile the device program

    gpu_start_time = time()  # Get the GPU start time
    event = program.sum(queue, (MAX_WIDTH,), None, a_buffer, t_buffer, x_buffer, np.uint32(MAX_WIDTH))  # Enqueue the GPU sum program XXX
    event.wait()  # Wait until the event finishes XXX
    gpu_end_time = time()  # Get the GPU end time
    x_gpu = np.empty_like(a)  # Create an empty array the same size as array a
    t_gpu = np.empty_like(a)  # Create an empty array the same size as array a
    a_gpu = np.empty_like(a)  # Create an empty array the same size as array a
    cl.enqueue_read_buffer(queue, x_buffer, x_gpu).wait()  # Read back the data from GPU memory into array c_gpu
    cl.enqueue_read_buffer(queue, a_buffer, a_gpu).wait()  # Read back the data from GPU memory into array c_gpu
    cl.enqueue_read_buffer(queue, t_buffer, t_gpu).wait()  # Read back the data from GPU memory into array c_gpu
    timediff = gpu_end_time - gpu_start_time
    #print("GPU Time: {0} s".format(gpu_end_time - gpu_start_time))  # Print the time the GPU program took, including both memory copies
    return a_gpu,t_gpu,x_gpu,timediff

######################################################################################################

# Test program starts here
##########################

timesum = []

a_gpu1,b_gpu1,x_gpu1,timediff = gpu_array_sum_naive(a,b,x)  # Call the function that sums two arrays on the GPU

print("\n\nI) Naive computation :")
print("###################")
print("Time taken in seconds was : " + str(timediff))
print("I) Dimensions of modified a array is " + str(a_gpu1.shape) )
print("Modified a was: {}".format(a_gpu1))

for i in range(len(a_gpu1)):
    print("\tI) Iteration #" + str(i) + "; a is " + str(a_gpu1[i]) + "; b is " + str(b_gpu1[i]) + "; x is " + str(x_gpu1[i]))

t_gpu,x_gpu,timediff = gpu_array_sum_improved_part1(t,x)  # Call the function that sums two arrays on the GPU

timesum.append(timediff)

print("\n\nII) Parallelized computation part 1:")
print("#######################")
print("Time taken in seconds was : " + str(timediff))
print("II) Dimensions of modified a array is " + str(t_gpu.shape))

print("Modified t was: {}".format(t_gpu))
print("Modified x was: {}".format(x_gpu))

t = t_gpu

b_gpu,x_gpu,timediff = gpu_array_sum_improved_part2(b,x)  # Call the function that sums two arrays on the GPU

timesum.append(timediff)

print("\n\nII) Parallelized computation part 2:")
print("#######################")
print("Time taken in seconds was : " + str(timediff))
print("II) Dimensions of modified a array is " + str(b_gpu.shape))

print("Modified b was: {}".format(b_gpu))
print("Modified x was: {}".format(x_gpu))

x = x_gpu

a_gpu,t_gpu,x_gpu,timediff = gpu_array_sum_improved_part3(a,t,x)  # Call the function that sums two arrays on the GPU

timesum.append(timediff)

print("\n\nIII) Parallelized computation part 3:")
print("#######################")
print("Time taken in seconds was : " + str(timediff))
print("II) Dimensions of modified a array is " + str(a_gpu.shape))

print("Modified a was: {}".format(a_gpu))
print("Modified b was: {}".format(b_gpu))
print("Modified x was: {}".format(x_gpu))
print("Modified t was: {}".format(t_gpu))

print("Total time taken for parallelized computation is " +str(sum(timesum)))

a_diff = a_gpu - a_gpu1
b_diff = b_gpu - b_gpu1
x_diff = x_gpu - x_gpu1

print("\n\nDifference in results of I and II")
print("##################################")
print("a_diff is {}".format(a_diff))
print("b_diff is {}".format(b_diff))
print("x_diff is {}".format(x_diff))

######################################################################################################
