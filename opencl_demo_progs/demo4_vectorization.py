#################################################################
#
#          .==.        .==.
#         //`^\\      //^`\\
#        // ^ ^\(\__/)/^ ^^\\
#       //^ ^^ ^/6  6\ ^^ ^^\\
#      //^ ^^ ^ ( .. ) ^ ^^^ \\
#     // ^^ ^/\//v""v\\/\^ ^ ^\\         JIFFY!
#    // ^^/\/  / `~~` \  \/\^ ^\\        ~~~~~~
#    \\^ /    / ,    , \    \^ //
#     \\/    ( (      ) )    \//
#      ^      \ \.__./ /      ^
#             (((`  ')))
#
#
#  filename : demo4_vectorization.py
#
#  purpose  : Script to demonstrate vectorization.
#
#################################################################

# Example from Slide #3 :
'''

    for (i = 1 to N)
    {
        X(i) = X(i) + Y(i)
    }

This program can be vectorized as :

    X(1:N) = X(1:N) + Y(1:N)

'''

from time import time              # Import time tools
import sys
sys.path.append('../')
import pyopencl as cl              # Import the OpenCL GPU computing API
import numpy as np                 # Import number tools
import logging
from analysis_tools import base

######################################################################################################

numeric_level = getattr(logging, "INFO", None)
logging.basicConfig(level=numeric_level)
logging.info("\n")

print("\n\nDEMO4 : Performing dependency checks")
print("####################################")

getdepsobj = base.getdeps()
getdepsobj.reset()
getdepsobj.setlooptype("sreg")
getdepsobj.addindexvar("i",1,100,1)

getdepsobj.addref("lhs", 1, "A", ["i"])
getdepsobj.addref("rhs", 1, "A", ["i"])

#getdepsobj.addref("lhs", 1, "A", ["i+1"])
#getdepsobj.addref("rhs", 1, "A", ["i"])
#getdepsobj.addref("lhs", 2, "B", ["i"])
#getdepsobj.addref("rhs", 2, "B", ["i"])

#getdepsobj.addref("lhs", 1, "A", ["i"])
#getdepsobj.addref("rhs", 2, "A", ["i"])
#getdepsobj.addref("lhs", 2, "B", ["i+1"])
#getdepsobj.addref("rhs", 1, "B", ["i"])
#getdepsobj.addref("lhs", 3, "C", ["i"])
#getdepsobj.addref("rhs", 3, "C", ["i"])

getdepsobj.dump()
getdepsobj.calcdeplist()

print("\n\nDEMO4 : Performing transformation analyses")
print("##########################################")
getdepsobj.check_transformations()

######################################################################################################

MAX_WIDTH  = 4

a = np.full(MAX_WIDTH,0x0).astype(np.int32)  # Create an array to add
b = np.full(MAX_WIDTH,0x0).astype(np.int32)  # Create an array to add
c = np.full(1,0x0).astype(np.int32)  # Create a random array to add

for i in range(MAX_WIDTH):
    a[i] = i+1
    b[i] = i+2

for i in range(MAX_WIDTH):
    print("INIT : Iteration #{}: a[{}] is {}, b[{}] is {}".format(i,i,a[i],i,b[i]))

######################################################################################################

def gpu_array_sum_naive(a, b):
    context = cl.create_some_context()  # Initialize the Context
    queue = cl.CommandQueue(context, properties=cl.command_queue_properties.PROFILING_ENABLE)  # Instantiate a Queue with profiling (timing) enabled
    a_buffer = cl.Buffer(context, cl.mem_flags.READ_WRITE | cl.mem_flags.COPY_HOST_PTR, hostbuf=a)
    b_buffer = cl.Buffer(context, cl.mem_flags.READ_WRITE | cl.mem_flags.COPY_HOST_PTR, hostbuf=b)
    program = cl.Program(context, """
    __kernel void sum(__global int *a, __global int *b, unsigned width)
    {
        int i = 0;
        int j = 0;

        for (i = 0; i<width; i++)
        {
            a[i] = a[i] + b[i];
        }

    }""").build()  # Compile the device program

    gpu_start_time = time()  # Get the GPU start time
    event = program.sum(queue, c.shape, None, a_buffer, b_buffer, np.uint32(MAX_WIDTH))  # Enqueue the GPU sum program XXX
    event.wait()  # Wait until the event finishes XXX
    gpu_end_time = time()  # Get the GPU end time
    c_gpu = np.empty_like(a)  # Create an empty array the same size as array a
    d_gpu = np.empty_like(b)  # Create an empty array the same size as array a
    cl.enqueue_read_buffer(queue, a_buffer, c_gpu).wait()  # Read back the data from GPU memory into array c_gpu
    cl.enqueue_read_buffer(queue, b_buffer, d_gpu).wait()  # Read back the data from GPU memory into array c_gpu
    timediff = gpu_end_time - gpu_start_time
    #print("GPU Time: {0} s".format(gpu_end_time - gpu_start_time))  # Print the time the GPU program took, including both memory copies
    return c_gpu,d_gpu,timediff

######################################################################################################

def gpu_array_sum_improved(a, b):
    context = cl.create_some_context()  # Initialize the Context
    queue = cl.CommandQueue(context, properties=cl.command_queue_properties.PROFILING_ENABLE)  # Instantiate a Queue with profiling (timing) enabled
    a_buffer = cl.Buffer(context, cl.mem_flags.READ_WRITE | cl.mem_flags.COPY_HOST_PTR, hostbuf=a)
    b_buffer = cl.Buffer(context, cl.mem_flags.READ_WRITE | cl.mem_flags.COPY_HOST_PTR, hostbuf=b)
    program = cl.Program(context, """
    __kernel void sum(__global int4 *a, __global int4 *b)
    {
        int global_addr = get_global_id(0);

        a[global_addr] = a[global_addr] + b[global_addr];

    }""").build()  # Compile the device program

    gpu_start_time = time()  # Get the GPU start time
    event = program.sum(queue, (1,), None, a_buffer, b_buffer)  # Enqueue the GPU sum program XXX
    event.wait()  # Wait until the event finishes XXX
    gpu_end_time = time()  # Get the GPU end time
    c_gpu = np.empty_like(a)  # Create an empty array the same size as array a
    d_gpu = np.empty_like(b)  # Create an empty array the same size as array a
    cl.enqueue_read_buffer(queue, a_buffer, c_gpu).wait()  # Read back the data from GPU memory into array c_gpu
    cl.enqueue_read_buffer(queue, b_buffer, d_gpu).wait()  # Read back the data from GPU memory into array c_gpu
    timediff = gpu_end_time - gpu_start_time
    #print("GPU Time: {0} s".format(gpu_end_time - gpu_start_time))  # Print the time the GPU program took, including both memory copies
    return c_gpu,d_gpu,timediff

######################################################################################################

# Test program starts here
##########################

c_gpu,d_gpu,timediff = gpu_array_sum_naive(a, b)  # Call the function that sums two arrays on the GPU

print("\n\nI) Naive computation :")
print("###################")
print("Time taken in seconds was : " + str(timediff))
print ("I) Dimensions of modified a & b arrays are " + str(c_gpu.shape) + " and " + str(d_gpu.shape))
print("Modified a was: {}".format(c_gpu))
print("Modified b was: {}".format(d_gpu))

#for i in range(len(c_gpu)):
#    print("\tI) Iteration #" + str(i) + "; a is " + str(c_gpu[i]) + "; b is " + str(d_gpu[i]))

e_gpu,f_gpu,timediff = gpu_array_sum_improved(a, b)  # Call the function that sums two arrays on the GPU

print("\n\nII) Vectorized computation :")
print("#######################")
print("Time taken in seconds was : " + str(timediff))
print ("II) Dimensions of modified a & b arrays are " + str(e_gpu.shape) + " and " + str(f_gpu.shape))

print("Modified a was: {}".format(e_gpu))
print("Modified b was: {}".format(f_gpu))

#for i in range(len(c_gpu)):
#    print("\tII) Iteration #" + str(i) + "; a is " + str(e_gpu[i]) + "; b is " + str(f_gpu[i]))

a_diff = c_gpu - e_gpu
b_diff = d_gpu - f_gpu

print("\n\nDifference in results of I and II")
print("##################################")
print("a_diff is {}".format(a_diff))
print("b_diff is {}".format(b_diff))
