#include <stdio.h>
#include <string.h>

int main()
{
    int i = 0;
    int A[10], X[10], B[10];

    memset(A, 0x0, sizeof(A));
    memset(B, 0x0, sizeof(A));
    memset(X, 0x0, sizeof(A));

    for (i = 0; i < 10; i++)
    {
        A[i] = i*3;
        B[i] = i+10;
        X[i] = i*2;
        printf("I) Iter # %d : A is %d, B is %d, X is %d\n", i, A[i], B[i], X[i]);
    }

    for (i = 0; i < 9; i++)
    {
        A[i] = X[i+1] + X[i];
        X[i+1] = B[i];
    }
    
    for (i = 0; i < 10; i++)
        printf("II) Iter # %d : A is %d, B is %d, X is %d\n", i, A[i], B[i], X[i]);

    return 0;
}
