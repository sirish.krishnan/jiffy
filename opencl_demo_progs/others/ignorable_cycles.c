#include <stdio.h>
#include <string.h>

int main()
{
    int i = 0;
    int a[12];
    memset(a,0x0,sizeof(a));

    for (i = 0; i<12; i++)
    {
        a[i] = i*10;
        printf("I) a[%d] is %d\n", i, a[i]);
    }

    for (i = 0; i<12; i++)
    {
        if (i < 8)
        {
            a[i] = a[i+4]*10;
        }
        printf("II) a[%d] is %d\n", i, a[i]);
    }

    return 0;
}
