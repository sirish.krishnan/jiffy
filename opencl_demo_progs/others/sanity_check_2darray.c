#include <stdio.h>
#include <string.h>

#if 1
int main()
{
    int i=0,j=0;
    int a[10][10];
    int b[10][10];

    memset(a, 0x0, sizeof(a));
    memset(b, 0x0, sizeof(b));

    for (i=0; i<10; i++)
    {
        for (j = 0; j<10; j++)
        {
            a[i][j] = 0x90;
            b[i][j] = 0x50;
        }
    }

    for (i = 2; i<10; i++)
    {
        for (j = 2; j<10; j++)
        {
            a[i][j] = b[i][j] + 2;
            b[i][j] = a[i][j-1] - b[i][j];
        }
    }

    for (i=0; i<10; i++)
    {
        for (j = 0; j<10; j++)
        {
            printf("II) i is %d, j is %d, a[i][j] is %d, b[i][j] is %d\n", i,j,a[i][j],b[i][j]);
        }
    }

    return 0;
}
#else
#define WIDTH  10
#define HEIGHT 10
int main()
{
    int i=0,j=0;
    int a[10][10];
    int b[10][10];
    int c[100];
    int d[100];
    int count = 0;
    int *temp_a = NULL, *temp_b = NULL;

#if 0
    for (i = 0; i<10; i++)
    {
        for (j = 0; j<10; j++)
        {
            a[i][j] = count;
            b[i][j] = count;
            count++;
        }
    }

    memcpy(c,a,sizeof(a));
    memcpy(d,b,sizeof(d));

    printf("Reached here atleast!1\n");

    for (i = 0; i<10; i++)
    {
        for (j = 0; j<10; j++)
        {
            printf("c[i][j] is %d\n", *(c+(WIDTH*i)+j));
        }
    }
#endif
    memset(c,0x90,sizeof(c));
    memset(d,0x50,sizeof(d));

    for (i = 2; i<10; i++)
    {
        for (j = 2; j<10; j++)
        {
            *(c+i*WIDTH+j) = *(d+i*WIDTH+j) + 2;
            *(d+i*WIDTH+j) = *(c+i*WIDTH+j-1) - *(d+i*WIDTH+j);
        }
    }

    for (i = 0; i<10; i++)
    {
        for (j = 0; j<10; j++)
        {
            printf("i is %d, j is %d, a[i][j] is %d, b[i][j] is %d\n",i,j, *(c+(WIDTH*i)+j), *(d+(WIDTH*i)+j));
        }
    }

    return 0;
}
#endif
