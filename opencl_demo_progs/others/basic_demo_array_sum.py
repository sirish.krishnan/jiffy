# Test the speed of your PyOpenCL program
from time import time  # Import time tools

import pyopencl as cl  # Import the OpenCL GPU computing API
import numpy as np  # Import number tools
 
a = np.random.rand(1000).astype(np.float32)  # Create a random array to add
b = np.random.rand(1000).astype(np.float32)  # Create a random array to add
r = np.random.rand(1).astype(np.float32)     # Create a random array to add

def gpu_array_sum(a, b):
    context = cl.create_some_context()  # Initialize the Context
    queue = cl.CommandQueue(context, properties=cl.command_queue_properties.PROFILING_ENABLE)  # Instantiate a Queue with profiling (timing) enabled
    a_buffer = cl.Buffer(context, cl.mem_flags.READ_ONLY | cl.mem_flags.COPY_HOST_PTR, hostbuf=a)
    b_buffer = cl.Buffer(context, cl.mem_flags.READ_ONLY | cl.mem_flags.COPY_HOST_PTR, hostbuf=b)
    c_buffer = cl.Buffer(context, cl.mem_flags.WRITE_ONLY, b.nbytes)  # Create three buffers (plans for areas of memory on the device)
    program = cl.Program(context, """
    __kernel void sum(__global const float *a, __global const float *b, __global float *c)
    {
        int i = get_global_id(0);
        int j;
        c[i] = a[i] + b[i];
    }""").build()  # Compile the device program
    event = program.sum(queue, a.shape, None, a_buffer, b_buffer, c_buffer)  # Enqueue the GPU sum program XXX
    event.wait()  # Wait until the event finishes XXX
    c_gpu = np.empty_like(a)  # Create an empty array the same size as array a
    cl.enqueue_read_buffer(queue, c_buffer, c_gpu).wait()  # Read back the data from GPU memory into array c_gpu
    
    d_buffer = cl.Buffer(context, cl.mem_flags.READ_ONLY | cl.mem_flags.COPY_HOST_PTR, hostbuf=c_gpu)
    r_buffer = cl.Buffer(context, cl.mem_flags.WRITE_ONLY, r.nbytes)  # Create three buffers (plans for areas of memory on the device)

    program = cl.Program(context, """
    __kernel void sum(__global const float *c, __global float *d)
    {
        int i = get_global_id(0);
        int j = 0;
        float sum = 0.0;
        for (j = 0; j<1000; j++)
        {
            sum = sum + c[j];
        }
        d[0] = sum;
    }""").build()  # Compile the device program
    event = program.sum(queue, c_gpu.shape, None, d_buffer, r_buffer)  # Enqueue the GPU sum program XXX
    event.wait()  # Wait until the event finishes XXX
    cl.enqueue_read_buffer(queue, r_buffer, r).wait()  # Read back the data from GPU memory into array c_gpu
    print("c_gpu: {}".format(c_gpu))  
    print("r: {}".format(r))  
    return c_gpu

c_gpu = gpu_array_sum(a, b)  # Call the function that sums two arrays on the GPU

sm = 0.0
for i in range(1000):
    sm = sm + c_gpu[i]

print("Verified sum was: {}".format(sm))
