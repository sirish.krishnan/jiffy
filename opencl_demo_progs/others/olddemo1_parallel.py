# Test the speed of your PyOpenCL program
from time import time  # Import time tools

import pyopencl as cl  # Import the OpenCL GPU computing API
import numpy as np  # Import number tools
 
MAX_WIDTH  = 10
MAX_HEIGHT = 10

a = np.full(MAX_WIDTH*MAX_HEIGHT,0x90).astype(np.int32)  # Create an array to add
b = np.full(MAX_WIDTH*MAX_HEIGHT,0x50).astype(np.int32)  # Create an array to add
c = np.full(1,0x0).astype(np.int32)  # Create a random array to add

######################################################################################################

def gpu_array_sum_naive(a, b):
    context = cl.create_some_context()  # Initialize the Context
    queue = cl.CommandQueue(context, properties=cl.command_queue_properties.PROFILING_ENABLE)  # Instantiate a Queue with profiling (timing) enabled
    a_buffer = cl.Buffer(context, cl.mem_flags.READ_WRITE | cl.mem_flags.COPY_HOST_PTR, hostbuf=a)
    b_buffer = cl.Buffer(context, cl.mem_flags.READ_WRITE | cl.mem_flags.COPY_HOST_PTR, hostbuf=b)
    program = cl.Program(context, """
    __kernel void sum(__global int *a, __global int *b, unsigned width, unsigned height)
    {
        int i = 0;
        int j = 0;

        for (i = 2; i<height; i++)
        {
            for (j = 2; j<width; j++)
            {   
                *(a+i*width+j) = *(b+i*width+j) + 2;
                *(b+i*width+j) = *(a+i*width+j-1) - *(b+i*width+j);
            }
        }

    }""").build()  # Compile the device program

    gpu_start_time = time()  # Get the GPU start time
    event = program.sum(queue, c.shape, None, a_buffer, b_buffer, np.uint32(MAX_WIDTH), np.uint32(MAX_HEIGHT))  # Enqueue the GPU sum program XXX
    event.wait()  # Wait until the event finishes XXX
    c_gpu = np.empty_like(a)  # Create an empty array the same size as array a
    d_gpu = np.empty_like(b)  # Create an empty array the same size as array a
    cl.enqueue_read_buffer(queue, a_buffer, c_gpu).wait()  # Read back the data from GPU memory into array c_gpu
    cl.enqueue_read_buffer(queue, b_buffer, d_gpu).wait()  # Read back the data from GPU memory into array c_gpu
    gpu_end_time = time()  # Get the GPU end time
    timediff = gpu_end_time - gpu_start_time
    #print("GPU Time: {0} s".format(gpu_end_time - gpu_start_time))  # Print the time the GPU program took, including both memory copies
    return c_gpu,d_gpu,timediff

######################################################################################################

def gpu_array_sum_improved(a, b):
    context = cl.create_some_context()  # Initialize the Context
    queue = cl.CommandQueue(context, properties=cl.command_queue_properties.PROFILING_ENABLE)  # Instantiate a Queue with profiling (timing) enabled
    a_buffer = cl.Buffer(context, cl.mem_flags.READ_WRITE | cl.mem_flags.COPY_HOST_PTR, hostbuf=a)
    b_buffer = cl.Buffer(context, cl.mem_flags.READ_WRITE | cl.mem_flags.COPY_HOST_PTR, hostbuf=b)
    program = cl.Program(context, """
    __kernel void sum(__global int *a, __global int *b, unsigned width)
    {
        int i = get_global_id(0);
        int j = 0;
        
        if (i>=2)
        {
            for (j = 2; j<width; j++)
            {   
                *(a+i*width+j) = *(b+i*width+j) + 2;
                *(b+i*width+j) = *(a+i*width+j-1) - *(b+i*width+j);
            }
        }

    }""").build()  # Compile the device program

    gpu_start_time = time()  # Get the GPU start time
    event = program.sum(queue, (MAX_HEIGHT,), None, a_buffer, b_buffer, np.uint32(MAX_WIDTH))  # Enqueue the GPU sum program XXX
    event.wait()  # Wait until the event finishes XXX
    c_gpu = np.empty_like(a)  # Create an empty array the same size as array a
    d_gpu = np.empty_like(b)  # Create an empty array the same size as array a
    cl.enqueue_read_buffer(queue, a_buffer, c_gpu).wait()  # Read back the data from GPU memory into array c_gpu
    cl.enqueue_read_buffer(queue, b_buffer, d_gpu).wait()  # Read back the data from GPU memory into array c_gpu
    gpu_end_time = time()  # Get the GPU end time
    timediff = gpu_end_time - gpu_start_time
    #print("GPU Time: {0} s".format(gpu_end_time - gpu_start_time))  # Print the time the GPU program took, including both memory copies
    return c_gpu,d_gpu,timediff

######################################################################################################

# Test program starts here
##########################

c_gpu,d_gpu,timediff = gpu_array_sum_naive(a, b)  # Call the function that sums two arrays on the GPU

print("\n\nI) Naive computation :")
print("###################")
print("Time taken in seconds was : " + str(timediff))
print ("I) Dimensions of modified a & b arrays are " + str(c_gpu.shape) + " and " + str(d_gpu.shape))
print("Modified a was: {}".format(c_gpu))
print("Modified b was: {}".format(d_gpu))

#for i in range(len(c_gpu)):
#    print("\tI) Iteration #" + str(i) + "; a is " + str(c_gpu[i]) + "; b is " + str(d_gpu[i]))

e_gpu,f_gpu,timediff = gpu_array_sum_improved(a, b)  # Call the function that sums two arrays on the GPU

print("\n\nII) Improved computation :")
print("#######################")
print("Time taken in seconds was : " + str(timediff))
print ("II) Dimensions of modified a & b arrays are " + str(c_gpu.shape) + " and " + str(d_gpu.shape))

print("Modified a was: {}".format(c_gpu))
print("Modified b was: {}".format(d_gpu))

#for i in range(len(c_gpu)):
#    print("\tII) Iteration #" + str(i) + "; a is " + str(e_gpu[i]) + "; b is " + str(f_gpu[i]))

a_diff = c_gpu - e_gpu
b_diff = d_gpu - f_gpu

print("\n\nDifference in results of I and II")
print("##################################")
print("a_diff is {}".format(a_diff))
print("b_diff is {}".format(b_diff))
