
          .==.        .==.
         //`^\\      //^`\\
        // ^ ^\(\__/)/^ ^^\\
       //^ ^^ ^/6  6\ ^^ ^^\\
      //^ ^^ ^ ( .. ) ^ ^^^ \\
     // ^^ ^/\//v""v\\/\^ ^ ^\\         JIFFY!
    // ^^/\/  / `~~` \  \/\^ ^\\        ~~~~~~
    \\^ /    / ,    , \    \^ //
     \\/    ( (      ) )    \//
      ^      \ \.__./ /      ^
             (((`  ')))


# What is Jiffy ?
~~~~~~~~~~~~~~~~~

Jiffy is a set of tools to perform hierarchical dependence analysis for subscripted variables, 
useful for performing concurrentization and other performance improvements on C code.

It includes :

    - A dependency checker (which uses GCD & Banerjee's tests to determine the dependencies
      which are RAW/WAR/WAW between statements)

    - A transformation validity checker to tests for the following transformations :
        - Concurrentization / Parallelization
        - Vectorization
        - Node splitting

    - Some test scripts to exercise Jiffy as well as example PyOpenCL test scripts to test
      transformed code wherever the transformation checker has detected a viable
      transformation.


# How does one run Jiffy ?
~~~~~~~~~~~~~~~~~~~~~~~~~~

Jiffy can be run on any machine provided the following tools / dependencies have been installed :

    - Python version 3.x.x
    - SymPy library
    - NumPy library

In addition to the base analysis tools, if you wish to run the PyOpenCL test code, the following
additional dependencies are also required :

    - A graphics card or equivalent computation unit which is supporte by OpenCL.
    - OpenCL software 
    - PyOpenCL library for Python 3.x.x


# Tips for installing dependencies
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

For installing Python libraries, you could use the PIP package manager which is provided
for Python, simply run the following command to install any Python library package :

    # pip3 install <package name>


# How to run the test code 
~~~~~~~~~~~~~~~~~~~~~~~~~~

To test the dependency checker, simply execute the script "depchecker_test.py" housed inside
"analysis_tools/" subfolder :
    
    # python3 depchecker_test.py

Ensure that one or more of the examples is enabled by toggling the "enable" flag checked
before executing each test case; prior to executing the script.

To test the transformed demo code (housed inside "opencl_demo_progs/" subfolder) via OpenCL, 
simply run the following commands :

    # export PYOPENCL_CTX=0
    # python3 <demo script>
